<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>

<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>
body {
      font-family: 'Lato', sans-serif;
   /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
  }
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

</style>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
      
      <?php include('sidebarList.php');?> 
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>
 

    <div class="col-md-12">
      <div class="box box-primary" style="min-height:570px;margin-top: 30px;">
        <div>
        <h6 style="margin-top:10px;margin-left: 15px;" class="page-header"><?php echo Lang::get('content.non_moving_report'); ?></h6>

        <div class="pull-right" style="margin-top: -12px; margin-right: 40px;">
            <img style="cursor: pointer;" ng-click="exportData('nonMovingReport')"  src="../app/views/reports/image/xls.png" />
            <img style="cursor: pointer;" ng-click="exportDataCSV('nonMovingReport')"  src="../app/views/reports/image/csv.jpeg" />
        </div>

          <div style="margin-left: 80px;"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?> : &nbsp;&nbsp;&nbsp;&nbsp;{{uiGroup}}</div>
          <div style="font-size: 14px; float: right; margin-right: 300px; margin-top: -20px;">
          <?php echo Lang::get('content.duration'); ?> :&nbsp;&nbsp;&nbsp;&nbsp;
            <select ng-model="durTime" ng-change="changeDuration(durTime)" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;">
              <option value="5">  > 5 <?php echo Lang::get('content.Hrs'); ?> </option>
              <option value="10">  > 10 <?php echo Lang::get('content.Hrs'); ?> </option>
              <option value="20">  > 20 <?php echo Lang::get('content.Hrs'); ?> </option>
              <option value="30"> > 30 <?php echo Lang::get('content.Hrs'); ?> </option>
            </select>
          </div>

        <div class="box-body" id="nonMovingReport">
            <div class="empty" align="center"></div>

              <table class="table table-striped table-bordered table-condensed table-hover" id="table_address" style="margin-top: 15px;">

                  <tr style="text-align:center">
                    <th width="13%" class="id" custom-sort order="'shortName'" sort="sort" style="text-align:center;background-color:#e4f6f9;">{{vehiLabel}} <?php echo Lang::get('content.name'); ?></th>
                    <th width="10%" class="id" custom-sort order="'regNo'" sort="sort" style="text-align:center;background-color:#e4f6f9;"><?php echo Lang::get('content.reg_no'); ?></th>
                    <th width="15%" class="id" custom-sort order="'date'" sort="sort" style="text-align:center;background-color:#e4f6f9;"><?php echo Lang::get('content.last_seen'); ?></th>
                    <th width="10%" class="id" custom-sort order="'position'" sort="sort" style="text-align:center;background-color:#e4f6f9;"><?php echo Lang::get('content.position'); ?></th>
                    <th width="12%" class="id" custom-sort order="'position'" sort="sort" style="text-align:center;background-color:#e4f6f9;"><?php echo Lang::get('content.duration'); ?> <?php echo Lang::get('content.h:m:s'); ?></th>
                    <th width="35%" class="id" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#e4f6f9;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                    <th width="10%" style="text-align:center;background-color:#e4f6f9;"><?php echo Lang::get('content.gmap'); ?></th>
                </tr>

                <tr ng-repeat="data in nonMovingData  | orderBy:natural(sort.sortingOrder):sort.reverse" class="active" style="text-align:center; font-size: 11px">
                    
                    <td>{{data.shortName}}</td>
                    <td>{{data.regNo}}</td>
                    <td>{{data.date | date:'yyyy-MM-dd HH:mm:ss'}}</td>
           
                    <td ng-switch on="data.position">
                        <span ng-switch-when="S"><img title="Vehicle Standing" src="assets/imgs/orange.png"/></span>
                        <span ng-switch-when="P"><img title="Vehicle Parked" src="assets/imgs/flag.png"/></span>
                        <span ng-switch-when="U"><img title="Vehicle NoData" src="assets/imgs/gray.png"/></span>
                    </td>
         
                    <td ng-switch on="data.position">
                        <span ng-switch-when="S">{{msToTime(data.idleTime)}}</span>
                        <span ng-switch-when="P">{{msToTime(data.parkedTime)}}</span>
                        <span ng-switch-when="U">{{msToTime(data.noDataTime)}}</span>
                    </td>
                                      
                    <td ng-if="data.address!=null" address={{data.address}}>{{data.address}}</td>
                    <td style="cursor: pointer;" get-location lat={{data.latitude}} lon={{data.longitude}} index={{$index}} ng-if="data.address==null && mainlist[$index]==null"><?php echo Lang::get('content.click_me'); ?></td>
                    <td style="cursor: pointer;" ng-if="data.address==null && mainlist[$index]!=null">{{mainlist[$index]}}</td>
                    <td><a href="https://www.google.com/maps?q=loc:{{data.latitude}},{{data.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>    

                  </tr>

                  <tr ng-if="nonMovingData==null || nonMovingData.length==0">
                    <td colspan="15" class="err"><h5><?php echo Lang::get('content.no_data_found'); ?></h5></td>
                  </tr>

                </table>  

              </div>
            </div>

          </div>
        </div>

      </div>
    </div>


    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/nonMovingReport.js"></script>
    
    <script>

   
        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


  </script>
    
</body>
</html>
