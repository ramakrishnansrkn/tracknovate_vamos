<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title> 
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>

body {
      font-family: 'Lato', sans-serif;
   /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif; */
}

.empty {
   height: 1px; width: 1px; padding-right: 30px; float: left;
}

.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
   background-color: #ffffff;
}

.tableId > tbody > tr > td {
   font-weight: unset !important;
}

</style>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
        <?php include('sidebarList.php');?> 
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>

    <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title"><?php echo Lang::get('content.fuel_consolidated'); ?></h3>
                </div>
               <div class="col-md-3" align="center"></div>
               <div class="row">
                    <div class="col-md-1" align="center"></div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">

                           <div style="margin-top: 8px;"><?php echo Lang::get('content.select'); ?> <?php echo Lang::get('content.date'); ?>:</div>
                        </div>                       
                    </div>
                 
                    <div class="col-md-2" align="center">
                        <div class="form-group">

                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>                       
                    </div>
 <!--                   <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
<!--                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
<!--                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.totime" class="form-control placholdercolor" id="timeTo" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
<!--                            </div>
                        </div>
                    </div> -->
                    <!--
                    <div class="col-md-1" align="center">
                        <div class="form-group">
                            
                                <select class="input-sm form-control" ng-model="interval">
                                     <option value="">Interval</option>
                                     <option label="1 mins">1</option>
                                     <option label="2 mins">2</option>
                                     <option label="5 mins">5</option>
                                     <option label="10 mins">10</option>
                                     <option label="15 mins">15</option>
                                     <option label="30 mins">30</option>
                                </select>
                           
                        </div>
                    </div>
                    -->
                    <div class="col-md-1" align="center"></div>
                    <div class="col-md-1" align="center">
                        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                </div>

              <!--  </div> -->
              <div class="row">
              <div class="col-md-1" align="center"></div>

               <div class="col-md-2" align="center">
                        <div class="form-group">

                         </div>
                  </div>
              </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-primary" style="min-height:570px;">            
          
                   <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">                     
                        <img style="cursor: pointer;" ng-click="exportData('FuelConReport')"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('FuelConReport')"  src="../app/views/reports/image/csv.jpeg" />
                    </div>            
              
                        <div class="box-body" id="FuelConReport">

                          <p style="margin-left: 60px;"><span><b><?php echo Lang::get('content.organization'); ?> <?php echo Lang::get('content.name'); ?> :<b> {{orgName}}</span> &nbsp;&nbsp;&nbsp;&nbsp;<span style="margin-left: 40px;"><b><?php echo Lang::get('content.date'); ?></b> : &nbsp;{{uiDate.fromdate}}</span> </p>

                            <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top:20px;">
                              <thead>
                                                     
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total_fuel_consumption'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{fuelConsumption}}</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.tot_fuel_fill'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{fuelFilling}}</th>
                                </tr>
                                <tr style="text-align:center;">                   
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_distance'); ?></th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{parseInts(dist)}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelTheft}}</th>
                                </tr>
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_engine_idle_hrs'); ?></th>
                                    <th ng-if="engineIdleHrs != 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{msToTime2(engineIdleHrs)}}</th>
                                    <th ng-if="engineIdleHrs == 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_engine_hrs'); ?></th>
                                    <th ng-if="ignitionHrs != 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{msToTime2(ignitionHrs)}}</th>
                                    <th ng-if="ignitionHrs == 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                                </tr>
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.KMPL'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalKmpl(dist,fuelConsumption)}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.LTPH'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalLtph(fuelConsumption,ignitionHrs)}}</th> 
                                   <!-- <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">-</th>-->
                                </tr>

                           <!-- <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Odo Distance</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.odoDistance}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total Trip Distance (Kms)</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.tripDistance}}</th>
                                </tr> -->

                              </thead>
                           </table>   
                           <div style="overflow-y: auto;">
                           <table class="table table-striped table-bordered table-condensed table-hover" style="margin-top:20px;">                                          
                                 <thead>                               
                                      <tr>
                                        <th class="id" custom-sort order="'vehicleName'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.veh_name'); ?></b></th>
                                        <th class="id" custom-sort order="'orgId'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.organization'); ?> <?php echo Lang::get('content.name'); ?></b></th>
                                        <th class="id" custom-sort order="'startFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_fuel'); ?></b></th>
                                        <th class="id" custom-sort order="'endFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_fuel'); ?></b></th>
                                        <th class="id" custom-sort order="'fuelConsumption'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_consumption'); ?></b></th>
                                        <th class="id" custom-sort order="'fuelFilling'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_filling'); ?></b></th>
                                        <th class="id" custom-sort order="'fuelTheft'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_theft'); ?></b></th>
                                        <th class="id" custom-sort order="'startKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_kms'); ?></b></th>
                                        <th class="id" custom-sort order="'endKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_kms'); ?></b></th>
                                        <th class="id" custom-sort order="'dist'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dis_travelled'); ?></b></th>
                                        <th class="id" custom-sort order="'kmpl'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.kmpl'); ?></b></th>   
                                   <!-- <th style="font-size:12px;background-color:#ecf7fb;"><b>Engine Start Hrs</b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b>Engine End Hrs</b></th>-->
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.running_hrs'); ?></b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.eng_idle_hrs'); ?></b></th>
                                        <th class="id" custom-sort order="'ltrsPerHrs'" sort="sort"  style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.ltrs_per_hrs'); ?></b></th>   
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.eng_hrs'); ?></b></th> 
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.remarks'); ?></b></th>                              
                                      </tr>
                                  </thead>

                                   <tbody>

                                   <span id="tableId">
                                     
                                      <tr ng-if="!conFuelError" ng-repeat="data in fuelConData | orderBy:natural(sort.sortingOrder):sort.reverse">                  
                                        <td style="font-weight: normal !important;">{{data.vehicleName}}</td>
                                        <td style="font-weight: normal !important;">{{data.orgId}}</td>
                                        <td style="font-weight: normal !important;">{{data.startFuel}}</td>
                                        <td style="font-weight: normal !important;">{{data.endFuel}}</td>
                                        <td style="font-weight: normal !important;">{{data.fuelConsumption}}</td>
                                        <td style="font-weight: normal !important;">{{data.fuelFilling}}</td>
                                        <td style="font-weight: normal !important;">{{data.fuelTheft}}</td>
                                        <td style="font-weight: normal !important;">{{parseInts(data.startKms)}}</td>
                                        <td style="font-weight: normal !important;">{{parseInts(data.endKms)}}</td>
                                        <td style="font-weight: normal !important;">{{data.dist}}</td>
                                        <td style="font-weight: normal !important;">{{data.kmpl}}</td>
                                   <!-- <td>-</td>
                                        <td>-</td>-->
                                        <td style="font-weight: normal !important;">{{msToTime2(data.engineRunningHrs)}}</td>
                                        <td style="font-weight: normal !important;">{{msToTime2(data.engineIdleHrs)}}</td>
                                        <td style="font-weight: normal !important;">{{data.ltrsPerHrs}}</td>
                                        <td style="font-weight: normal !important;">{{msToTime2(data.ignitionHrs)}}</td>
                                        <td style="font-weight: normal !important;">{{data.remarks}}</td>
                                      </tr>

                                    </span>  

                                      <tr ng-if="!conFuelError">                  

                                        <td colspan="2" style="font-size: 13px;font-weight: bold;"><?php echo Lang::get('content.total'); ?></td>                                       
                                   <!-- <td style="font-size: 13px;font-weight: bold;">{{startFuel}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{endFuel}}</td> -->
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelConsumption}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelFilling}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelTheft}}</td>
                                   <!-- <td style="font-size: 13px;font-weight: bold;">{{parseInts(startKms)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{parseInts(endKms)}}</td> -->
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{parseInts(dist)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{totalKmpl(dist,fuelConsumption)}}</td>                                   
                                        <td ng-if="engineHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineHrs)}}</td>
                                        <td ng-if="engineHrs == 'NaN'" style="font-size: 13px;font-weight: bold;">-</td>
                                        <td ng-if="engineIdleHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineIdleHrs)}}</td>
                                        <td ng-if="engineIdleHrs == 'NaN'" style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{totalLtph(fuelConsumption,ignitionHrs)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{msToTime2(ignitionHrs)}}</td>
                                        <td>-</td>
                                      </tr>
                      
                                    <tr ng-if="conFuelError" style="text-align: center">
                                        <td colspan="21" class="err"><h5>{{conFuelError}}</h5></td>
                                    </tr>

                                    <!-- <tr ng-if="fuelConData.length==0" style="text-align: center">
                                        <td colspan="21" class="err"><h5><?php echo Lang::get('content.no_date_time'); ?></h5></td>
                                    </tr> -->
                          </table> 
                                                 
                        </div>

                </div>
            </div>

          </div>
        </div>

    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
<!--<script src="assets/js/highcharts.js"></script> -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/fuelConsolidate.js"></script>
    
    <script>

   
        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


  </script>
    
</body>
</html>
