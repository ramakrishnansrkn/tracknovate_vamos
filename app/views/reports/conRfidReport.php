<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>
body {
      font-family: 'Lato', sans-serif;
   /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
  }
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

</style>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
      <?php include('sidebarList.php');?>
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>
 
    <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title"><?php echo Lang::get('content.cons_rfid_report'); ?></h3>
                </div>
               <div class="row">
                    <div class="col-md-1" align="center"></div>
                    <?php include('dateTime.php');?>
                    <div class="col-md-1" align="center"></div>
                     <div class="col-md-1" align="center">
                        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                </div>

              <!--  </div> -->
              <div class="row">
              <div class="col-md-1" align="center"></div>

               <div class="col-md-2" align="center">
                        <div class="form-group">

                         </div>
                  </div>
              </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-primary" style="min-height:570px;">
        
                <div>
                    <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">
                        <img style="cursor: pointer;" ng-click="exportData('ConRfidReport')"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('ConRfidReport')"  src="../app/views/reports/image/csv.jpeg" />
                    </div>

                                  
                      <div class="box-body" id="ConRfidReport">
                      
                            <table class="table table-striped table-bordered table-condensed table-hover" style="padding:10px 0 10px 0;">
                                          
                                <tbody ng-repeat="data in conRfidData" >
                                      <tr style="text-align:center;height:30px;">
                                        <td colspan="3" style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.vehicle_id'); ?> : {{data.vehicleId}}</b></td>
                                        <td colspan="3" style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.veh_name'); ?> : {{data.vehicleName}}</b></td>
                                      </tr>
                                  <!--<tr><td colspan="4" bgcolor="grey"></td><tr>-->
                                      <tr>
                                    <!--<td style="font-size:12px;background-color:#ecf7fb;"><b>Tag Id</b></td>-->                                    
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></b></td>
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></b></td>
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.tag_no'); ?></b></td>
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.tag_name'); ?></b></td>
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.loc'); ?></b></td>
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.gmap'); ?></b></td>
                                        <!-- <td width="10%" style="font-size:12px;background-color:#ecf7fb;"><b>Geofence</b></td> -->
                                      </tr>
                                     
                                      <tr ng-repeat="subdata in data.gd" style="padding-bottom:20px;" >
                                        <!-- <td>{{subdata.tagId}}</td> -->
                                         <td>{{subdata.fromTime | date:'HH:mm:ss'}}&nbsp;&nbsp;{{subdata.fromTime | date:'dd-MM-yyyy'}}</td>
                                         <td>{{subdata.toTime | date:'HH:mm:ss'}}&nbsp;&nbsp;{{subdata.toTime | date:'dd-MM-yyyy'}}</td>
                                         <td>{{splitTag(subdata.tagDetail,1)}}</td>
                                         <td>{{splitTag(subdata.tagDetail,2)}}</td>
                                         <td>{{subdata.address}}</td>
                                         <td><a href="https://www.google.com/maps?q=loc:{{subdata.lat}},{{subdata.lng}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>                      
                                         </tr>
                                      <tr ng-if="data.gd==null&&!conFuelerror" style="text-align: center">
                                        <td colspan="6" class="err"><h5>{{data.error}}</h5></td>
                                      </tr>

                                      <tr><td colspan="6" bgcolor="grey"></td><tr>
                                 </tbody>
 
                                  <tr ng-if="conRfidData.length==0" style="text-align: center">
                                    <td colspan="6" class="err"><h5><?php echo Lang::get('content.no_date_time'); ?></h5></td>
                                  </tr>
                                  <tr ng-if="conFuelerror" style="text-align: center">
                                    <td colspan="6" class="err"><h5>{{conFuelerror}}</h5></td>
                                  </tr>
                            </table>
                        
                        </div>
                   
                    </div>
                </div> 
            </div>

          </div>
        </div>


    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/conRfidReport.js"></script>
    
    <script>

        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

  </script>
    
</body>
</html>
