<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title> 
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>
body {
      font-family: 'Lato', sans-serif;
   /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
}
.empty{
   height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
   background-color: #ffffff;
}

</style>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
      <?php include('sidebarList.php');?> 
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>

    <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title"><?php echo Lang::get('content.vehicle_wise_report'); ?></h3>
                </div>
          
               <div class="row">
                    <div class="col-md-1" align="center"></div>

                    <div class="col-md-2" align="center">
                        <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                          <h5 style="color: grey;">{{shortNam}}</h5>
                        </div>
                       <!-- <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                          <h5 style="color: red;">Vehicle not found</h5>
                        </div> -->
                    </div>
                 
                    <div class="col-md-2" align="center">
                        <div class="form-group">

                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>                       
                    </div>
                 <!--   <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                     <!--      </div>
                        </div> 
                    </div>-->
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>
                    </div>
                   <!--<div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.totime" class="form-control placholdercolor" id="timeTo" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                    <!--        </div>
                        </div>
                    </div> -->
                    <!--
                    <div class="col-md-1" align="center">
                        <div class="form-group">
                            
                                <select class="input-sm form-control" ng-model="interval">
                                     <option value="">Interval</option>
                                     <option label="1 mins">1</option>
                                     <option label="2 mins">2</option>
                                     <option label="5 mins">5</option>
                                     <option label="10 mins">10</option>
                                     <option label="15 mins">15</option>
                                     <option label="30 mins">30</option>
                                </select>
                           
                        </div>
                    </div>
                    -->
                    <div class="col-md-1" align="center"></div>
                    <div class="col-md-1" align="center">
                        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                </div>

              <!--  </div> -->
              <div class="row">
              <div class="col-md-1" align="center"></div>

               <div class="col-md-2" align="center">
                        <div class="form-group">

                         </div>
                  </div>
              </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-primary" style="min-height:570px;"> 

              <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">                     
                        <img style="cursor: pointer;" ng-click="exportData('VehicleWiseFuelReport')"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('VehicleWiseFuelReport')"  src="../app/views/reports/image/csv.jpeg" />
                    </div>                           
              
                        <div class="box-body" id="VehicleWiseFuelReport">

                            <p style="margin-left: 60px;"><span><b><?php echo Lang::get('content.organization'); ?> <?php echo Lang::get('content.name'); ?> :</b> {{orgName}}</span> &nbsp;&nbsp;&nbsp;&nbsp;<span style="margin-left: 40px;"><b><?php echo Lang::get('content.fromdate'); ?></b> : &nbsp;{{uiDate.fromdate}}</span> <span style="margin-left: 40px;"><b><?php echo Lang::get('content.todate'); ?></b> : &nbsp;{{uiDate.todate}}</span> </p>

                            <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top:20px;" ng-if="fuelConData[0].error==null">
                              <thead>
                               <tr style="text-align:center;">
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{gName}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_name'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{shortNam}}</th>                               
                                </tr>                        
                            
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_consumption'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{fuelConsumption}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_fill'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{fuelFilling}}</th>
                                </tr>
                                <tr style="text-align:center;">                   
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total_distance'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{parseInts(dist)}}</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{fuelTheft}}</th>
                                </tr>
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_engine_idle_hrs'); ?></th>
                                    <th ng-if="engineIdleHrs != 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{msToTime2(engineIdleHrs)}}</th>
                                    <th ng-if="engineIdleHrs == 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_engine_hrs'); ?></th>
                                    <th ng-if="ignitionHrs != 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{msToTime2(ignitionHrs)}}</th>
                                    <th ng-if="ignitionHrs == 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                                </tr>
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.KMPL'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalKmpl(dist,fuelConsumption)}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.LTPH'); ?></th>
                                <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalLtph(fuelConsumption,ignitionHrs)}}</th> 
                                  <!--  <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">-</th>-->
                                </tr>
                        <!--   <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Odo Distance</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.odoDistance}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total Trip Distance (Kms)</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.tripDistance}}</th>
                                </tr> -->
                              </thead>
                           </table>   
                 
                           <table class="table table-striped table-bordered table-condensed table-hover" style="margin-top:20px;">                                          
                                 <thead>                               
                                      <tr>
                                        <td class="id" custom-sort order="'date'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.date'); ?></b></td>                                        
                                        <td class="id" custom-sort order="'startFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_fuel'); ?></b></td>
                                        <td class="id" custom-sort order="'endFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_fuel'); ?></b></td>
                                        <td class="id" custom-sort order="'fuelConsumption'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_consumption'); ?></b></td>
                                        <td class="id" custom-sort order="'fuelFilling'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_filling'); ?></b></td>
                                        <td class="id" custom-sort order="'fuelTheft'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_theft'); ?></b></td>
                                        <td class="id" custom-sort order="'startKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_kms'); ?></b></td>
                                        <td class="id" custom-sort order="'endKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_kms'); ?></b></td>
                                        <td class="id" custom-sort order="'dist'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dis_travelled'); ?></b></td>
                                        <td class="id" custom-sort order="'kmpl'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.kmpl'); ?></b></td>   
                                    <!--<td style="font-size:12px;background-color:#ecf7fb;"><b>Engine Start Hrs</b></td>
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b>Engine End Hrs</b></td>-->
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.running_hrs'); ?></b></td>
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.engine_idle_hrs'); ?></b></td>
                                        <td class="id" custom-sort order="'ltrsPerHrs'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.ltrs_per_hrs'); ?></b></td>   
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.engine_hrs'); ?></b></td> 
                                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.remarks'); ?></b></td>                              
                                      </tr>
                                  </thead>

                                   <tbody>                                    
                                      <tr ng-if="fuelConData[0].error==null" ng-repeat="data in fuelConData | orderBy:natural(sort.sortingOrder):sort.reverse">                  
                                        <td>{{data.date}}</td>
                                        <td>{{data.startFuel}}</td>
                                        <td>{{data.endFuel}}</td>
                                        <td>{{data.fuelConsumption}}</td>
                                        <td>{{data.fuelFilling}}</td>
                                        <td>{{data.fuelTheft}}</td>
                                        <td>{{parseInts(data.startKms)}}</td>
                                        <td>{{parseInts(data.endKms)}}</td>
                                        <td>{{data.dist}}</td>
                                        <td>{{data.kmpl}}</td>
                                    <!--<td>-</td>
                                        <td>-</td>-->
                                        <td>{{msToTime2(data.engineRunningHrs)}}</td>
                                        <td>{{msToTime2(data.engineIdleHrs)}}</td>
                                        <td>{{data.ltrsPerHrs}}</td>
                                        <td>{{msToTime2(data.ignitionHrs)}}</td>
                                        <td>{{data.remarks}}</td>
                                      </tr>

                                      <tr ng-if="fuelConData[0].error==null">                  

                                        <td style="font-size: 13px;font-weight: bold;"><?php echo Lang::get('content.total'); ?></td>                                       
                                   <!-- <td style="font-size: 13px;font-weight: bold;">{{startFuel}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{endFuel}}</td> -->
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelConsumption}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelFilling}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelTheft}}</td>
                                   <!-- <td style="font-size: 13px;font-weight: bold;">{{parseInts(startKms)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{parseInts(endKms)}}</td> -->
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td> 
                                        <td style="font-size: 13px;font-weight: bold;">{{parseInts(dist)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{totalKmpl(dist,fuelConsumption)}}</td>                                   
                                        <td ng-if="engineHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineHrs)}}</td>
                                        <td ng-if="engineHrs == 'NaN'" style="font-size: 13px;font-weight: bold;">-</td>
                                        <td ng-if="engineIdleHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineIdleHrs)}}</td>
                                        <td ng-if="engineIdleHrs == 'NaN'" style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{totalLtph(fuelConsumption,ignitionHrs)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{msToTime2(ignitionHrs)}}</td>
                                        <td>-</td>
                                      </tr>
                      
                                      <tr ng-if="fuelConData[0].error!=null" style="text-align: center">
                                        <td colspan="20" class="err"><h5>{{fuelConData[0].error}}</h5></td>
                                      </tr>

                                 <!-- <tr ng-if="fuelConData.length==0" style="text-align: center">
                                        <td colspan="20" class="err"><h5>No Data Found! Choose some other date and time!</h5></td>
                                      </tr> -->
                          </table> 
                                                 
                        </div>


                </div>
            </div>



          </div>
        </div>

    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
<!--<script src="assets/js/highcharts.js"></script> -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/fuelConsolidate2.js"></script>
    
    <script>

   
        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


  </script>
    
</body>
</html>
