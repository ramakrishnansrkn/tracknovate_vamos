<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>

<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>

  body{
     font-family: 'Lato', sans-serif;
   /*font-weight: bold;*/  
   /*font-family: 'Lato', sans-serif;
     font-family: 'Roboto', sans-serif;
     font-family: 'Open Sans', sans-serif;
     font-family: 'Raleway', sans-serif;
     font-family: 'Faustina', serif;
     font-family: 'PT Sans', sans-serif;
     font-family: 'Ubuntu', sans-serif;
     font-family: 'Droid Sans', sans-serif;
     font-family: 'Source Sans Pro', sans-serif;*/
  }

.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}

.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
    background-color: #ffffff;
}

</style>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
        <?php include('sidebarList.php');?>
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>
 
    <!-- AdminLTE css box-->

    <div class="col-md-12">
       <div class="box box-primary">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title"><?php echo Lang::get('content.radio_frequency'); ?> </h3>
                </div>
                <div class="row">
                    <div class="col-md-2" align="center">
                        <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                          <h5 style="color: grey;">{{shortNam}}</h5>
                        </div>
                     
                  </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.totime" class="form-control placholdercolor" id="timeTo" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-1" align="center">
                        <div class="form-group">
                                <select class="input-sm form-control" ng-model="interval">
                                     <option value="">Interval</option>
                                     <option label="1 mins">1</option>
                                     <option label="2 mins">2</option>
                                     <option label="5 mins">5</option>
                                     <option label="10 mins">10</option>
                                     <option label="15 mins">15</option>
                                     <option label="30 mins">30</option>
                                </select>
                        </div>
                    </div> -->
                    <div class="col-md-1" align="center"></div>
                     <div class="col-md-1" align="center">
                        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                </div>

              <!--  </div> -->
            </div>
            
          
        </div>

        <div class="col-md-12">
            <div class="box box-primary">
                <div>
                    <div class="pull-right">
                        <img style="cursor: pointer;" ng-click="exportData('rfidreport')"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('rfidreport')"  src="../app/views/reports/image/csv.jpeg" />
                        <!-- <img style="cursor: pointer;" onClick ="$('#tableID').tableExport({type:'pdf',escape:'false'});"  src="assets/imgs/red.png" /> -->
                    </div>
                  
                <div class="box-body" id="rfidreport">
                <div class="empty" align="center"></div>

                <div class="form-group pull-right" style="padding-right: 42px;margin-top: 0px;">
                        <input type="search" class="form-control input-sm" placeholder="Search" align="center" ng-model="rfidSearch" name="search" />
                 </div> 
                
                <p style="margin:0;font-size:18px;" ><?php echo Lang::get('content.rfid_tag'); ?> <span style="float: right;font-size:15px;padding-right: 50px;margin-top: 3px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{uiDate.fromdate}} &nbsp;{{convert_to_24hrs(uiDate.fromtime)}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{uiDate.todate}} &nbsp;{{convert_to_24hrs(uiDate.totime)}}</span></p> 

                
                <div class="row">
                    <div class="col-md-1" align="center"></div>
                    <div class="col-md-2" align="center">
                        <div class="form-group"></div>
                    </div>
                </div>   

                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                        <tr style="text-align:center; font-weight: bold; ">
                            <td style="background-color:#ecf7fb;"><?php echo Lang::get('content.veh_name'); ?></td>
                            <td style="background-color:#f9f9f9;">{{shortNam}}</td> 
                            <td style="background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></td>
                            <td colspan="2" style="background-color:#f9f9f9;">{{uiGroup}}</td>
                        </tr>

                    <tr><td colspan="5"></td></tr>
                    <tr style="text-align:center;font-weight: bold;">
                        <th class="id" custom-sort order="'dateTime'" sort="sort" style="text-align:center;background-color:#d2dff7;" width="15%"><?php echo Lang::get('content.date_time'); ?></th>
                        <th class="id" custom-sort order="'tagNo'" sort="sort" style="text-align:center;background-color:#d2dff7;" width="18%"><?php echo Lang::get('content.tag_no'); ?></th>
                        <th class="id" custom-sort order="'tagNam'" sort="sort" style="text-align:center;background-color:#d2dff7;" width="17%"><?php echo Lang::get('content.tag_name'); ?></th> 
                        <th class="id" custom-sort order="'address'" sort="sort" style="text-align: center;background-color:#d2dff7;" width="40%"><?php echo Lang::get('content.loc'); ?></th>
                        <th class="id" style="text-align: center;background-color:#d2dff7;" width="10%"><?php echo Lang::get('content.gmap'); ?></th>
                    </tr>
                    </thead>
                                <tbody ng-repeat="rfid in rfiData | orderBy:sort.sortingOrder:sort.reverse | filter:rfidSearch">
                                    <tr class="active" style="text-align:center">
                                        <td>{{rfid.dateTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                        <td>{{rfid.tagNo}}</td>
                                        <td>{{rfid.tagNam}}</td>
                                        <td>{{rfid.address}}</td>
                                        <td><a href="https://www.google.com/maps?q=loc:{{rfid.lat}},{{rfid.lng}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                    </tr>
                                </tbody>
                                <tr ng-if="rfiData == null || rfiData.length == 0"  align="center">
                                    <td colspan="5" class="err" ng-if="!error"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                    <td colspan="5" class="err" ng-if="error"><h5>{{error}}</h5></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="assets/js/ui-bootstrap-0.6.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places" type="text/javascript"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/siteReport.js"></script>
    <script>

        // $("#example1").dataTable();
        // $("#menu-toggle").click(function(e) {
        //     e.preventDefault();
        //     $("#wrapper").toggleClass("toggled");
        // });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


  </script>
    
</body>
</html>
