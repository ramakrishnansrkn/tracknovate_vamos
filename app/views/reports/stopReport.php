<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

body{

font-family: 'Lato', sans-serif;
/*font-weight: bold;*/  

/* font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
} 

</style>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
        <?php include('sidebarList.php');?> 
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>
 


    <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title"><?php echo Lang::get('content.stoppage'); ?> <?php echo Lang::get('content.report'); ?></h3>
                </div>
               <div class="row">
                    <div class="col-md-1" align="center"></div>
                   <?php include('dateTime.php');?>
                    <div class="col-md-1" align="center"></div>
                     <div class="col-md-1" align="center">
                        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                </div>
              <!--  </div> -->
              <div class="row">
              <div class="col-md-1" align="center"></div>

               <div class="col-md-2" align="center">
                        <div class="form-group">

                         </div>
                  </div>
              </div>

            <div class="row" ng-init="geoFence='Y'" style="padding-bottom:7px;">

              <div class="col-md-2" align="center"></div>
                
             <!-- <div class="col-md-1" align="center">
                        <div class="form-group" style="padding-right: 0px;font-size:14px;">
                      Group :
                         </div>
                  </div>   
                  <div class="col-md-2" >
                        <div class="form-group">

                          <select ng-model="gName" ng-change="groupSelection(gName)" style="background-color:#f9f9f9;padding:3px 3px 3px 3px;min-width:150px;max-width:150px;height:25px;">
                              <option ng-repeat="vehi in vehicle_group" value="{{vehi.vgName}}">{{trimColon(vehi.vgName)}}</option>
                            </select>
                         
                        </div>
                  </div>  -->

                  <div class="col-md-2" align="center">
                      <input checked="checked" type="radio" ng-model="geoFence" value="Y" style="padding-left: 20px;" />
                      <span style="font-size: 12px;padding:0 0 0 5px;font-weight: bold; "><?php echo Lang::get('content.all'); ?></span>
                  </div>
                  
                  <div class="col-md-2" >
                      <input style="padding-left: 20px;" type="radio" ng-model="geoFence" value="Yes" />
                      <span style="font-size: 12px;padding:0 0 0 10;font-weight: bold; "><?php echo Lang::get('content.scheduled'); ?></span>
                  </div>

                  <div class="col-md-2" style="padding:unset;" >
                      <input type="radio" ng-model="geoFence" value="No" />
                      <span style="font-size: 12px;padding:0 0 0 0;font-weight: bold; "><?php echo Lang::get('content.unscheduled'); ?></span>
                  </div>
                 <!-- <div class="empty" align="center"></div> -->
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-primary" style="min-height:570px;">
        
                <div>

                    <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">
                      <span style="font-size: 14px;padding-right: 22px;">
              
                                  <select ng-model="timeChanges" ng-change="filtersTime(timeChanges)" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;">
                                        <option value="All"> <?php echo Lang::get('content.all'); ?> </option>
                                        <option value="1">  >  1 <?php echo Lang::get('content.min'); ?>  </option>
                                        <option value="2">  >  2 <?php echo Lang::get('content.Mins'); ?> </option>
                                        <option value="5">  >  5 <?php echo Lang::get('content.Mins'); ?> </option>
                                        <option value="10"> > 10 <?php echo Lang::get('content.Mins'); ?> </option>
                                        <option value="15"> > 15 <?php echo Lang::get('content.Mins'); ?> </option>
                                        <option value="30"> > 30 <?php echo Lang::get('content.Mins'); ?> </option>
                                       <option value="100"> > 1 <?php echo Lang::get('content.hour'); ?>  </option>
                                   </select>
                              </span>
                        <img style="cursor: pointer;" ng-click="exportData('StoppageReport')"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('StoppageReport')"  src="../app/views/reports/image/csv.jpeg" />
                    </div>

                    <div class="form-group pull-right" style="padding-right: 42px;margin-top: 10px;">
                        <input type="search" class="form-control input-sm" placeholder="<?php echo Lang::get('content.search'); ?>" align="center" ng-model="searchbox2" name="search" />
                    </div>
                    
                  <div class="form-group pull-right" style="z-index:2;padding-right:15%;margin-top:8px;width:20px;height:6px;cursor:pointer;" ng-dropdown-multiselect="" options="selectVehiData" selected-model="selectVehiModel" ng-click="selectVehicle(selectVehiModel)"  extra-settings="example14settings" translation-texts="example14texts"></div>  

                <div class="box-body" id="StoppageReport">
                <div class="empty" align="center"></div>
               <table>
                      <tbody> 
                            <tr ng-show="showBanner">
                              <td style="font-size:14px;padding:0 0 0 85px"> <?php echo Lang::get('content.report'); ?> :</td>
                                <td  ng-if="geoFence=='Y'" style="font-size:14px;padding:0 0 0 7px"><span><?php echo Lang::get('content.all'); ?></span></td>
                                 <td ng-if="geoFence=='Yes'" style="font-size:14px;padding:0 0 0 7px"><span><?php echo Lang::get('content.scheduled'); ?></span></td>
                                  <td ng-if="geoFence=='No'" style="font-size:14px;padding:0 0 0 7px"><span><?php echo Lang::get('content.unscheduled'); ?></span></td>

                                <td style="font-size:14px;padding-left: 60px;"> <?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?> :</td>
                                <td style="font-size:14px;padding:0 0 0 7px;"> {{trimColon(gName)}}</td>
                            </tr>
                       </tbody> 
                      </table>

                        <div class="row" style="padding-top: 20px;"></div>

                        <table class="table table-striped table-bordered table-condensed table-hover" style="padding:10px 0 10px 0;">
                                          
                                <tbody ng-repeat="data in stopData" >
                                     <tr style="text-align:center;height:30px;">
                                        <td colspan="2" style="font-size:13px;background-color:#C2D2F2;"><b>{{vehiLabel}} <?php echo Lang::get('content.name'); ?> : {{data.shortName}}</b></td>
                                        <td style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.start_time'); ?> : {{data.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</b></td>
                                        <td colspan="2" style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.end_time'); ?> : {{data.endTime | date:'yyyy-MM-dd HH:mm:ss'}}</b></td>
                                      </tr>
                                 <!-- <tr><td colspan="4" bgcolor="grey"></td><tr> -->
                                      <tr>
                                        <td width="15%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_time'); ?></b></td>
                                        <td width="15%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_time'); ?></b></td>
                                        <td width="30%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.address'); ?></b></td>
                                        <td width="20%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.stoppage'); ?> <?php echo Lang::get('content.duration'); ?> (h:m:s)</b></td>
                                    <!--  <td width="10%" style="font-size:12px;background-color:#ecf7fb;"><b>Geofence</b></td> -->
                                        <td width="10%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.gmap'); ?></b></td>
                                      </tr>
                                     
                                      <tr ng-repeat="subdata in data.history | filter:searchbox2" style="padding-bottom:20px;">
                                         <td width="15%" ng-show="geoFence==subdata.isInsideGeofence || geoFence=='Y'">{{subdata.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                         <td width="15%" ng-show="geoFence==subdata.isInsideGeofence || geoFence=='Y'">{{subdata.endTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                         <td width="30%" ng-show="geoFence==subdata.isInsideGeofence || geoFence=='Y'">{{subdata.address}}</td>
                                         <td width="20%" ng-show="geoFence==subdata.isInsideGeofence || geoFence=='Y'">{{msToTime(subdata.stoppageTime)}}</td>
                                    <!-- <td width="10%" ng-show="geoFence==subdata.isInsideGeofence || geoFence=='Y'">{{subdata.isInsideGeofence}}</td>-->
                                         <td width="10%" ng-show="geoFence==subdata.isInsideGeofence || geoFence=='Y'"><a href="https://www.google.com/maps?q=loc:{{subdata.latitude}},{{subdata.langitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                      </tr>
                                      <tr ng-if="data.history==null" style="text-align: center">
                                        <td ng-if="data.error==null" colspan="5" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                        <td ng-if="data.error!=null" colspan="5" class="err"><h5>{{data.error}}</h5></td>
                                      </tr>
                                      <tr><td colspan="5" bgcolor="grey"></td><tr>
                                </tbody>
                          </table>                        
                        </div>
                    </div>
                </div>
            </div>

          </div>
        </div>


    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.7.0/lodash.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.9/angular.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/underscore.js"></script>
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/stoppageReport.js"></script>
    
    <script>

        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


  </script>
    
</body>
</html>