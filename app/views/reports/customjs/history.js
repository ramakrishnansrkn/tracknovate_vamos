var getIP = globalIP;
//var app = angular.module('hist',['ui.bootstrap']);

app.controller('histCtrl',['$scope', '$http', '$filter', '_global','$translate', function($scope, $http, $filter, GLOBAL,$translate) {
var language=localStorage.getItem('lang');
  $scope.multiLang=language;
  $translate.use(language);
var translate = $filter('translate');
 var assLabel          =  localStorage.getItem('isAssetUser');
 $scope.vehiAssetView  =  true;
 $scope.trvShow        =  localStorage.getItem('trackNovateView');
 $scope.dealerName     =  localStorage.getItem('dealerName');
 $scope.filterDuration=240000;
 $scope.dealerFilter   =  false;  
 if($scope.dealerName == 'FUELVIEW') {
   $scope.dealerFilter = true;  
 }
 $scope.tn       =  getParameterByName('tn'); 
 $scope.vehicleId =getParameterByName('vid');
//alert( localStorage.getItem('dealerName') );
  console.log('Dealer : '+$scope.dealerName);

  if( assLabel == "true" ) {
      $scope.vehiLabel      =  "Asset";
      $scope.vehiImage      =  true;
      $scope.vehiAssetView  =  false;
  } else if( assLabel == "false" ) {
      $scope.vehiLabel      =  "Vehicle";
      $scope.vehiImage      =  false;
      $scope.vehiAssetView  =  true;
  } else {
      $scope.vehiLabel      =  "Vehicle";
      $scope.vehiImage      =  false;
      $scope.vehiAssetView  =  true;
  }
  
 $scope.ignDurTot      =  "0h : 0m : 0s"; 
 $scope.reportBanShow  =  false;
 $scope.reportUrl      =  GLOBAL.DOMAIN_NAME+'/getReportsList';

 
 $scope.$watch("reportUrl", function (val) {
      
  $http.get($scope.reportUrl).success(function(data) {

     var tabShow = data;
      //console.log(tabShow);
      if(tabShow != "" && tabShow != null)  {     
     // console.log('not Empty getReportList API ...');
        angular.forEach(tabShow,function(val, key){

            var newReportName = Object.getOwnPropertyNames(val).sort();
            
            if(newReportName == 'Analytics_IndivdiualReportsList'){
              if(val.Analytics_IndivdiualReportsList[0].IDLE==false&&val.Analytics_IndivdiualReportsList[0].PARKED==false&&val.Analytics_IndivdiualReportsList[0].STOPPAGE==false&&val.Analytics_IndivdiualReportsList[0].MOVEMENT==false){

                  $scope.reportBanShow = true;
                  $(window).load(function(){
                      $('#allReport').modal('show');
                  });

                  stopLoading();

              } else {

                var reportSubName = Object.getOwnPropertyNames(val.Analytics_IndivdiualReportsList[0]).sort();
                  //console.log(reportSubName);

                angular.forEach(reportSubName,function(value, keys) {
                //console.log(value);
                  switch(value){
                    /* case 'AC':
                        //if(val.Analytics_IndivdiualReportsList[0].AC==false){$scope.tabAcreportShow = false; }
                        $scope.tabAcreportShow = val.Analytics_IndivdiualReportsList[0].AC;
                       break; */
                       case 'OVERSPEED':
                        //if(val.Analytics_IndivdiualReportsList[0].OVERSPEED==false){ $scope.tabOverspeedShow = false;  }
                        $scope.tabOverspeedShow = val.Analytics_IndivdiualReportsList[0].OVERSPEED;
                       break;
                       // case 'FUEL':
                       //  //if(val.Analytics_IndivdiualReportsList[0].FUEL==false){ $scope.tabFuelShow   = false;  }
                       //  $scope.tabFuelShow = val.Analytics_IndivdiualReportsList[0].FUEL;
                       // break;
                       case 'IDLE':
                        //if(val.Analytics_IndivdiualReportsList[0].IDLE==false){ $scope.tabIdleShow  = false; }
                        $scope.tabIdleShow  = val.Analytics_IndivdiualReportsList[0].IDLE;
                       break;
                       case 'PARKED':
                        //if(val.Analytics_IndivdiualReportsList[0].PARKED==false){$scope.tabParkedShow  = false; }
                        $scope.tabParkedShow = val.Analytics_IndivdiualReportsList[0].PARKED; 
                       break;
                       case 'STOPPAGE':
                        //if(val.Analytics_IndivdiualReportsList[0].STOPPAGE==false){ $scope.tabStopShow = false; }
                        $scope.tabStopShow = val.Analytics_IndivdiualReportsList[0].STOPPAGE;
                       break;
                       case 'MOVEMENT':
                        //if(val.Analytics_IndivdiualReportsList[0].MOVEMENT==false){ $scope.tabMovementShow = false;}
                        $scope.tabMovementShow = val.Analytics_IndivdiualReportsList[0].MOVEMENT;
                       break;
                       case 'IGNITION':
                        //if(val.Analytics_IndivdiualReportsList[0].IGNITION==false){ $scope.tabIgnitionShow = false; }
                        $scope.tabIgnitionShow = val.Analytics_IndivdiualReportsList[0].IGNITION;
                       break;
                    }
                });
              }
            }
        });

    } else {
      console.log('Empty getReportList API ...');

      $scope.tabStopShow        =  true;
      $scope.tabMovementShow    =  true;
      $scope.tabNoDataShow      =  true;
    //$scope.tabAcreportShow    =  true;
      $scope.tabIgnitionShow    =  true;
      //$scope.tabFuelShow        =  true;
      $scope.tabIdleShow        =  true;
      $scope.tabParkedShow      =  true;
      $scope.tabOverspeedShow   =  true; 
    }
})
.error(function(data, status) {
  console.error('Repos error', status, data);
   console.log('Empty getReportList API ...');
      
      $scope.tabStopShow        =  true;
      $scope.tabMovementShow    =  true;
      $scope.tabNoDataShow      =  true;
    //$scope.tabAcreportShow    =  true;
      $scope.tabIgnitionShow    =  true;
      //$scope.tabFuelShow        =  true;
      $scope.tabIdleShow        =  true;
      $scope.tabParkedShow      =  true;
      $scope.tabOverspeedShow   =  true; 
  });

});


  $scope.overallEnable     =   true;
  $scope.tabactive         =   true;
  $scope.cardData          =   false;
  $scope.showErrMsg        =   false;
  $scope.oaddress          =   [];
  $scope.maddress          =   [];
  $scope.maddress1         =   [];
  $scope.saddress          =   [];
  $scope.addressIdle       =   [];
  $scope.addressEvent      =   [];
  $scope.saddressStop      =   [];
  $scope.eventReportData   =   [];
  $scope.addressLoad       =   [];
  $scope.addressFuel       =   [];
//$scope.location          =   [];
  $scope.ltrs              =   [];
  $scope.fuelDate          =   [];
  $scope.gIndex            =   0;

  $scope.interval          =   getParameterByName('interval')?getParameterByName('interval'):"";
  $scope.sort              =   sortByDate('date');
     
//$scope.itemsPerPage      =   5;
//$scope.currentPage       =   0;
//$scope.items             =   [];

  $scope.filteredTodos     =   [];
  $scope.itemsPerPage      =   10;
  $scope.currentPage       =   1;
      
/*$scope.gap               =   5;
  $scope.itemsPerPage      =   5;
  $scope.currentPage       =   0;*/
  
  $scope.time_stop         =   'All';
  $scope.time_park         =   'All';
   
//$scope.timeDrops=[{'timeId' :'Above 5 mins' },{'timeId' :'Above 10 mins' },{'timeId' :'Above 15 mins' },{'timeId' :'Above 30 mins' },{'timeId' :'Above 1 hrs' }];
//console.log($scope.timeChange);

  function sessionValue(vid, gname){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    $("#testLoad").load("../public/menu");
  }

  $("#testLoad").load("../public/menu");
  // $scope.url = 'http://'+getIP+context+'/public//getVehicleLocations';
  $scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');

  $scope.$watch("url", function (val) {

    $http.get($scope.url).success(function(data) {

      //$scope.locations        =  data;
        $scope.vehicle_list     =  data;
        $scope.vehiname         =  getParameterByName('vid');
        $scope.uiGroup          =  $scope.trimColon(getParameterByName('vg'));
        $scope.gName            =  getParameterByName('vg');  

        //alert($scope.vehiname);

        if($scope.vehiname=='undefined' || $scope.vehiname==''){
          $scope.vehiname  =  data[$scope.gIndex].vehicleLocations[0].vehicleId; 

          //alert($scope.vehiname );
        } 

        if($scope.gName=='undefined' || $scope.gName==''){
          $scope.gName  =  data[$scope.gIndex].group; 
        } 

        angular.forEach(data, function(val, key) {

          if($scope.gName == val.group) {

            $scope.gIndex = val.rowId;       

            angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys) {

              if($scope.vehiname == value.vehicleId) {

                if(value.position == "N" || value.position == "Z"){
                   alert('Vehicle '+value.vehicleId+'is not synced!');
                }

                $scope.shortNam = value.shortName;
              }

            });
          }
            
        });
        
        sessionValue($scope.vehiname, $scope.gName);

      // if(data.length){
      //  $scope.vehiname   = data[0].vehicleLocations[0].vehicleId;
      //  $scope.gName    =   data[0].group; 
      //  // sessionValue($scope.vehiname, $scope.gName);
      //  angular.forEach(data, function(value, key) {
      //      if(value.totalVehicles) {
      //        $scope.data1    = data[key];
      //      }
      //  });   
      // }
    }).error(function(){ /*alert('error'); */ });
  });


    $scope.downloadid    =  'movementreport';
    var prodId           =  getParameterByName('vid');
    var tabId            =  getParameterByName('tn');
    $scope.tab_val       =  getParameterByName('tn');
    $scope.vgroup        =  getParameterByName('vg');
    $scope.dvgroup       =  getParameterByName('dvg');
    $scope.vvid          =  getParameterByName('vvid');
    $scope.repId         =  getParameterByName('rid');
    $scope.fd            =  getParameterByName('fd');
    $scope.ft            =  getParameterByName('ft');
    $scope.td            =  getParameterByName('td');
    $scope.tt            =  getParameterByName('tt');
    var ignitionValue    =  [];
    $scope.todayhistory  =  [];
    
    $scope.getTodayDate  =  function(date) {
      var date = new Date(date);
      return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
    };

  $scope.trimColon = function(textVal){

    var splitVal =[];
    
    if(textVal!=undefined){
    
       splitVal = textVal.split(/[:]+/);
  
      }else{
       splitVal[0]='No Value';
    }
    return splitVal[0];
   }

    function formatAMPM(date) {
        var date = new Date(date);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
  }

  $scope.convert_to_24hrs = function(time_str) {

   if(time_str != undefined){
      
      var str   = time_str.split(' ');
      var stradd  = str[0].concat(":00");
      var strAMPM = stradd.concat(' '+str[1]);
      var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
      var hours = Number(time[1]);
      var minutes = Number(time[2]);
      var seconds = Number(time[2]);
      var meridian = time[4].toLowerCase();
  
      if (meridian == 'p' && hours < 12) {
        hours = hours + 12;
      }
      else if (meridian == 'a' && hours == 12) {
        hours = hours - 12;
      } 

      hours   = hours < 10 ? '0'+hours : hours;
      minutes = minutes < 10 ? '0'+minutes : minutes;  
      seconds = seconds < 10 ? '0'+seconds : seconds; 

      var marktimestr = ''+hours+':'+minutes+':'+seconds;   
    }

  return marktimestr;
  };

function eventButton(eventdate)
  {
    $scope.buttonClick = eventdate;
    serviceCallEvent();
  }

  function serviceCallEvent()
    {
      var stoppage        =   document.getElementById ("stop").checked
      var idleEvent       =   document.getElementById ("idle").checked;
      var notReachable    =   document.getElementById ("notreach").checked;
      var overspeedEvent  =   document.getElementById ("overspeed").checked;
      var stopMints       =   document.getElementById ("stop1").value;
      var idleMints       =   document.getElementById ("idle1").value;
      var notReachMints   =   document.getElementById ("notreach1").value;
      var speedEvent      =   document.getElementById ("overspeed1").value
      var locEvent        =   document.getElementById ("location").checked;
      var siteEvent       =   document.getElementById ("site").checked;
      var urlEvent        =   GLOBAL.DOMAIN_NAME+"/getActionReport?vehicleId="+prodId+"&fromDate="+$scope.fromdate+"&fromTime="+convert_to_24h($scope.fromtime)+"&toDate="+$scope.todate+"&toTime="+convert_to_24h($scope.totime)+"&interval="+$scope.interval+"&stoppage="+stoppage+"&stopMints="+stopMints+"&idle="+idleEvent+"&idleMints="+idleMints+"&notReachable="+notReachable+"&notReachableMints="+notReachMints+"&overspeed="+overspeedEvent+"&speed="+speedEvent+"&location="+locEvent+"&site="+siteEvent;
      //console.log(' inside the method '+ urlEvent)

      $http.get(urlEvent).success(function(eventRes){
        $scope.eventReportData    = eventRes;
   //     $('#status').fadeOut(); 
      // $('#preloader').delay(350).fadeOut('slow');
      stopLoading();
        if($scope.buttonClick==true)
        {
          $scope.alertMe_click($scope.downloadid);
        
        }

      })
      
    }

  //for individual method for event report service call
  
  $scope.eventCall    =     function()
    {
      document.getElementById ("stop").checked        = true;
      document.getElementById ("idle").checked        = true;
      document.getElementById ("notreach").checked      = true;
      document.getElementById ("overspeed").checked     = true;
      document.getElementById ("stop1").defaultValue      = 10;
      document.getElementById ("idle1").defaultValue      = 10;
      document.getElementById ("notreach1").defaultValue    = 10;
      document.getElementById ("overspeed1").defaultValue   = 60;
      document.getElementById ("location").checked      = false;
      document.getElementById ("site").checked        = false;
      //buttonClick = false;
      serviceCallEvent();
      // return $scope.eventReportData;
    }

    //site web service
    $scope.siteCall       =     function()
    {
      var url =GLOBAL.DOMAIN_NAME+"/getSiteReport?vehicleId="+prodId+"&fromDate="+$scope.fromdate+"&fromTime="+convert_to_24h($scope.fromtime)+"&toDate="+$scope.todate+"&toTime="+convert_to_24h($scope.totime)+"&interval="+$scope.interval+"&site=true";
      // console.log(' asd '+url);
      $http.get(url).success(function(siteval){
        $scope.siteReport=[];
        $scope.siteReport = siteval;
   //     $('#status').fadeOut(); 
      // $('#preloader').delay(350).fadeOut('slow');
      stopLoading();
      });
    }

    
    $scope.$watch("tab_val", function (newval, oldval) {
    // $scope.$watch("tabId", function (val) {

   /* $scope.tabStopShow      = true;
      $scope.tabMovementShow  = true;
      $scope.tabAcreportShow  = true;
      $scope.tabIgnitionShow  = true;
      $scope.tabFuelShow      = true;
      $scope.tabIdleShow      = true;
      $scope.tabParkedShow    = true;
      $scope.tabOverspeedShow = true;*/
      $scope.tabAll         =  false;
      $scope.tabNodata      =  false;
      $scope.tabmovement    =  false;
      $scope.taboverspeed   =  false;
      $scope.tabparked      =  false;
      $scope.tabidle        =  false;
      $scope.tabevent       =  false;
      $scope.tabsite        =  false;
      $scope.tabload        =  false;
      $scope.tabfuel        =  false;
      $scope.tabignition    =  false;
   // $scope.tabac          =  false;
      $scope.tabStop        =  false;

      switch(tabId){
        case 'all':
          $scope.tabAll         = true; 
        break;
        case 'movement':
          $scope.tabmovement    = true; 
        break;
        case 'overspeed':
          $scope.taboverspeed   = true;
        break;
        case 'parked':
          $scope.tabparked      = true;
        break;
        case 'idle':
          $scope.tabidle        = true;
        break;
        case 'noData':
          $scope.tabNoData      = true;
        break;
        case 'event':
          $scope.tabevent       = true;
        break;
        case 'site':
          $scope.tabsite        = true;
        break;
        case 'load':
          $scope.tabload        = true;
        break;
        case 'fuel':
          $scope.tabfuel        = true;
        break;
        case 'ignition':
          $scope.tabignition     = true;
        break;
     /* case 'acreport':
          $scope.tabac           = true;
        break;*/
        case 'stoppageReport':
          $scope.tabStop        = true;
        break;
        default:
          $scope.tabmovement    = true; 
        break;
      }
    }, true);

     function getTodayDatess() {
      var date = new Date();
      return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
    };
    
    (function initial(prodId){
      console.log(' new  '+prodId)
      startLoading();

      $scope.id = prodId;


      $scope.fromTimes = localStorage.getItem('fromTime');
      $scope.fromDates = localStorage.getItem('fromDate');
      $scope.toTimes       = localStorage.getItem('toTime');
      $scope.toDates       = localStorage.getItem('toDate');
      if(localStorage.getItem('timeTochange')!='yes'){
                updateToTime();
                $scope.toTimes   =   localStorage.getItem('toTime');
                }
        
      var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromDates,$scope.fromTimes)+'&toDateUTC='+utcFormat($scope.toDates,$scope.toTimes);



      // $scope.fromTimes = "00:00:00";
      // $scope.fromDates = getTodayDatess();
        
      //    var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromDates,$scope.fromTimes);
      // var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+'&fromDateUTC='+utcFormat($scope.fromDates,$scope.fromTimes);
      // var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromDates,$scope.fromTimes);
      // var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+getParameterByName('vid')+"&interval="+$scope.interval;
    
      $scope.loading  = true;
      try{
        $http.get(histurl).success(function(data){

        //console.log(data.error);
          $scope.errMsg=data.error;
          $scope.showErrMsg = true;

        if(data.vehicleLocations != null) {

          $scope.loading        =  false;
          $scope.hist           =  data; 
          $scope.cardData       =  true;
          $scope.filterLoc      =  data.vehicleLocations;  
          $scope.topspeedtime   =  data.topSpeedTime;
        //$scope.tankSize       =  parseInt(data.tankValue);
          $scope.tankSize       =  data.tankValue;

        //$scope.dataGeofence(data.gfTrip);   
          var fromNow         = new Date(data.fromDateTime.replace('IST',''));
          var toNow           = new Date(data.toDateTime.replace('IST',''));
          $scope.fromNowTS    = data.fromDateTimeUTC;
          $scope.toNowTS      = data.toDateTimeUTC; 
          $scope.fromtime     = formatAMPM($scope.fromNowTS);
          $scope.totime       = formatAMPM($scope.toNowTS);
          $scope.fromdate     = $scope.getTodayDate($scope.fromNowTS);
          $scope.todate       = $scope.getTodayDate($scope.toNowTS);

          $scope.toTimeVal = utcFormat($scope.todate,convert_to_24h($scope.totime));

          $scope.dataArray(data.vehicleLocations);
          
        } else {
          
       //    var curDates        = new Date();   
       //    $scope.fromtime     = '12:00 AM';
       // // $scope.totime       = formatAMPM($scope.toNowTS);
       //    $scope.totime       = formatAMPM(curDates);
       //    $scope.fromdate     = getTodayDatess();
       //    $scope.todate       = getTodayDatess();
          $scope.fromtime     = localStorage.getItem('fromTime');
          $scope.totime       = localStorage.getItem('toTime');
          $scope.fromdate     = localStorage.getItem('fromDate');
          $scope.todate       = localStorage.getItem('toDate');
          if(localStorage.getItem('timeTochange')!='yes'){
                updateToTime();
                $scope.totime    =   localStorage.getItem('toTime');
                }

          $scope.toTimeVal = utcFormat($scope.todate,convert_to_24h($scope.totime));
        }
        // $scope.eventCall();
        // $scope.siteCall();
        stopLoading();
      });
    } catch (errr){
      // $('#status').fadeOut(); 
      // $('#preloader').delay(350).fadeOut('slow');  
      console.log(' error '+errr);
      stopLoading();
    }  
    }(prodId));

    $scope.alertMe_click    = function(value){
      switch(value){
        case 'movementreport':
          $scope.recursive1($scope.movementdata,0);
          break;
        case 'overspeedreport':
          $scope.recursive($scope.overspeeddata,0);
          break;
        case 'stoppedparkingreport':
          $scope.recursiveStop($scope.parkeddata,0);
          break;
        case 'idlereport':
          $scope.recursiveIdle($scope.idlereport,0);
          break;
        case 'eventReport':
          $scope.recursiveEvent($scope.eventReportData,0);
          break;
     /* case 'acreport':
          // $scope.recursiveLoad($scope.loadreport,0);
          break; */
        case 'fuelreport':
          $scope.fuelChart($scope.fuelValue);
          $scope.recursiveFuel($scope.fuelValue,0);
          break;
        default:
          break;
      }
    }

    function _pairFilter(_data, _yes, _no, _status) {

      var _checkStatus =_no, _pairList = [];

    angular.forEach(_data, function(value, key) {
        
        if( _pairList.length <= 0 ) {

              if(value[_status] == _yes) {
                _pairList.push(value)
              }

        } else if(_pairList.length >0 ) {

              if(value[_status] == _checkStatus) {

                  _pairList.push(value);
                      if($scope.dealerFilter){
                          try{
                           if((_pairList[_pairList.length-1].ignitionStatus == 'OFF')){
                            //alert(_pairList[_pairList.length-1].ignitionStatus);
                                 if(((_pairList[_pairList.length-1].date)-(_pairList[_pairList.length-2].date))<=$scope.filterDuration){
                                  
                                  _pairList.pop();
                                  //remdata=true;
                                   }
                               }
                            }catch(err){
                              console.log(err);
                            }
                          }
                  if(_pairList[_pairList.length-1][_status] == _yes) {
                      _checkStatus = _no;
                  } else {
                      _checkStatus = _yes
                  }
              }
        }

    });
     

     if(_status=='ignitionStatus') {
       console.log('filter Duration');
        var ignVar    = 0;
        var onInit    = 0;
        var ignVarTot = 0;

         for(var i=0; i<_pairList.length; i++){
            if(_pairList[i].ignitionStatus=="ON"){
               if(onInit==0){
                  ignVar=ignVar+_pairList[i].date;
                  onInit=1;
               }  
            } else if(_pairList[i].ignitionStatus=="OFF"){

                if(onInit==1){
                 ignVarTot =ignVarTot+(_pairList[i].date-ignVar);
                 ignVar=0;
                 onInit=0;
                }
            }
      } 
      //console.log($scope.msToTime2(ignVarTot));
        $scope.ignDurTot  = $scope.msToTime2(ignVarTot);
        $scope.ignDurTot2 = ignVarTot;
      }     

      console.log(_pairList);

      if(_pairList.length>1) {

        if(_pairList.length%2==0) {

           return _pairList;

           console.log(_pairList);

        } else {

           var lastVal = _pairList[_pairList.length-1];

           console.log( lastVal );

         //_pairList.pop();

           _pairList[_pairList.length] = { date:$scope.toTimeVal, ignitionStatus:"ONN", address:lastVal.address, latitude:lastVal.latitude, longitude:lastVal.longitude };
 
          console.log(_pairList); 

          if($scope.ignDurTot2!=undefined) {

          //console.log($scope.ignDurTot2);
          //console.log($scope.toTimeVal);
            console.log( _pairList[_pairList.length-2].date );

             var dur = $scope.toTimeVal - _pairList[_pairList.length-2].date;
                 dur = $scope.ignDurTot2 + dur;
              
            $scope.ignDurTot = $scope.msToTime2(dur);

               console.log( $scope.ignDurTot );
          }

          return _pairList;
        }

      }  else {
           //_pairList.pop();

         console.log(_pairList);

          //return _pairList;
      }

    }

  /*   function filterNoData(data){

      //console.log(data);
        console.log(data.length);
        var retArr  = data;

          var toDateTimeVal = utcFormat($scope.todate,convert_to_24h($scope.totime)); 

            retArr.push({date:toDateTimeVal,position:"UU"});

              console.log(retArr);

      return retArr;
     }*/


    // function ignitionFilter(ignitionValue)
    // {
    //  $scope.ignitionData   =_pairFilter(ignitionValue, 'ON', 'OFF', 'ignitionStatus');
    // }
    // function acFilter(_acData){
    //  $scope.acReport   =_pairFilter(_acData, 'yes', 'no', 'vehicleBusy');
    // }

   function loadReportApi(url){
      //var loadUrl = "http://"+getIP+context+"/public/getLoadReport?vehicleId="+prodId;
      $http.get(url).success(function(loadresponse){
        $scope.loadreport     =    loadresponse;
        console.log(' log '+$scope.tabload)
      });
    }

  /*  function filter(obj){
      var _returnObj = [];
      if(obj)
        angular.forEach(obj,function(val, key){
          if(val.fuelLitre >0)
            _returnObj.push(val)
        })
      return _returnObj;
    }*/

    function filter(obj,name){
      var _returnObj = [];
      if(name=='fuel'){
        angular.forEach(obj,function(val, key){

          if(val.fuelLitre >0)
          {
            _returnObj.push(val)
          }
        })
      }
      else if(name=='stoppage'){

        angular.forEach(obj,function(val, key){

          if(val.stoppageTime >0)
          {
            _returnObj.push(val)
          }
        })
      }
      else if(name=='ovrspd'){

        angular.forEach(obj,function(val, key){

          if(val.overSpeedTime >0)
          {
            _returnObj.push(val)
          }
        })
      }
    return _returnObj;
    }


    function _globalFilter(data) { 
      
      $scope.allData          = [];
      $scope.parkeddata       = [];
      $scope.overspeeddata    = [];
      $scope.movementdata     = [];
      $scope.idlereport       = [];
      $scope.temperatureData  = [];
      $scope.fuelValue        = [];
      $scope.ignitionData     = [];
    //$scope.acReport         = [];
      $scope.stopReport       = [];
      $scope.noData           = [];

      try {
        
        if(data || data.length > 0) {

          $scope.ovrSpeedGraph(data);

          $scope.allData         =  data;
          $scope.parkeddata      =  ($filter('filter')(data, {'position':"P"}));
          //$scope.overspeeddata =  ($filter('filter')(data, {'isOverSpeed':"Y"}));
          $scope.overspeeddata   =  filter(data,'ovrspd');
          $scope.movementdata    =  ($filter('filter')(data, {'position':"M"}));
          $scope.idlereport      =  ($filter('filter')(data, {'position':"S"}));
          $scope.temperatureData =  ($filter('filter')(data, {'temperature': "0"}));
          $scope.fuelValue       =  filter(data,'fuel');
          ignitionValue          =  ($filter('filter')(data, {'ignitionStatus': "!undefined"}))
          $scope.ignitionData    =  _pairFilter(ignitionValue, 'ON', 'OFF', 'ignitionStatus');
        //$scope.acReport        =  _pairFilter(data, 'yes', 'no', 'vehicleBusy');
          $scope.stopReport      =  filter(data,'stoppage');
          $scope.noData          =  ($filter('filter')(data, {'position':"U"}));
    
          // ignitionFilter(ignitionValue);
          // acFilter(data)
      }
    } catch (error) {
      stopLoading();
    }
  }


  $scope.timeFilter_park=function(data,fVal) { 

      var filterValues=fVal;
      var ret_obj=[];

      if(data){

          angular.forEach(data,function(val, key){

          if(val.parkedTime >0 && filterValues==100){

            if(val.parkedTime>=3600000){

              ret_obj.push(val);
            }

          }else if(val.parkedTime >0 && filterValues==30){


            if(val.parkedTime>=1800000){

              ret_obj.push(val);
            }

          }else if(val.parkedTime >0 && filterValues==15){

            if(val.parkedTime>=900000){

              ret_obj.push(val);
            }

          }else if(val.parkedTime >0 && filterValues==10){

            if(val.parkedTime>=600000){

              ret_obj.push(val);
            }
          }else if(val.parkedTime >0 && filterValues==5){


            if(val.parkedTime>=300000){

              ret_obj.push(val);
            }
          }else if(val.parkedTime >0 && filterValues==2){


            if(val.parkedTime>=120000){

              ret_obj.push(val);
            }
          }
          else if(val.parkedTime >0 && filterValues==1){


            if(val.parkedTime>=60000){

              ret_obj.push(val);
            }
          }else if(val.parkedTime >0 && filterValues=='All'){

              ret_obj.push(val);
          }
        

        })
      }
   
   return ret_obj;
  }

  $scope.timeFilter_stop=function(data,fVal) {

    var filterValues=fVal;
    var ret_obj=[];

     if(data){

          angular.forEach(data,function(val, key){

          if(val.stoppageTime>0 && filterValues==100){

            if(val.stoppageTime>=3600000){

              ret_obj.push(val);
            }

          }else if(val.stoppageTime >0 && filterValues==30){


            if(val.stoppageTime>=1800000){

              ret_obj.push(val);
            }

          }else if(val.stoppageTime >0 && filterValues==15){

            if(val.stoppageTime>=900000){

              ret_obj.push(val);
            }

          }else if(val.stoppageTime >0 && filterValues==10){

            if(val.stoppageTime>=600000){

              ret_obj.push(val);
            }
          }else if(val.stoppageTime >0 && filterValues==5){

            if(val.stoppageTime>=300000){

              ret_obj.push(val);
            }

          }else if(val.stoppageTime >0 && filterValues==2){

            if(val.stoppageTime>=120000){

              ret_obj.push(val);
            }

          }
          else if(val.stoppageTime >0 && filterValues==1){

            if(val.stoppageTime>=60000){

              ret_obj.push(val);
            }

          }else if(val.stoppageTime >0 && filterValues=='All'){

              ret_obj.push(val);
          }
        

        })
      }
   
   return ret_obj;
  }

 /*  $scope.filtersTime= function(val,name){

  $scope.filterValue=val;

  if(name=='park'){
  $scope.parkeddata=[];

  console.log('park');
  $scope.parkeddata=$scope.timeFilter_park($scope.filterLoc, $scope.filterValue);

  }else if(name=='stop'){
   console.log('stop');
     $scope.stopReport=[];
     $scope.stopReport=$scope.timeFilter_stop($scope.filterLoc, $scope.filterValue);
   }

  }  */


 $scope.filtersTime= function(val,name) {

 //  $scope.filterValue=val;
 //  $scope.filterValueName=name;
  
  if(name=='park'){
     // console.log('park');
      $scope.filterValPark=val;
      $scope.parkeddata=[];
      $scope.parkeddata=$scope.timeFilter_park($scope.filterLoc, $scope.filterValPark);
  }else if(name=='stop'){
    // console.log('stop');
     $scope.filterValStop=val;
     $scope.stopReport=[];
     $scope.stopReport=$scope.timeFilter_stop($scope.filterLoc, $scope.filterValStop);
   }
}

    //for initial loading
    $scope.dataArray      = function(data) {
      
      _globalFilter(data);


    if(tabId == 'fuel')
    {
      $scope.fuelChart($scope.fuelValue);
      $scope.recursiveFuel($scope.fuelValue, 0);
    }
    
    // loadReportApi("http://"+getIP+context+"/public/getLoadReport?vehicleId="+prodId);
    $scope.recursive1($scope.movementdata,0);
    //console.log(' data----> '+$scope.downloadid)
    };

  /*  // for submit button click
    $scope.dataArray_click    = function(data) {
      
      _globalFilter(data)
      $scope.alertMe_click($scope.downloadid);
    
    };*/

// for submit button click
    $scope.dataArray_click    = function(data) {

     $scope.time_stop='All';
     $scope.time_park='All'; 
      
      _globalFilter(data);

    if($scope.filterValPark!=undefined && $scope.filterValStop!=undefined){

      $scope.parkeddata=[];
      $scope.parkeddata=$scope.timeFilter_park($scope.filterLoc, $scope.filterValPark);

      $scope.stopReport=[];
      $scope.stopReport=$scope.timeFilter_stop($scope.filterLoc,$scope.filterValStop);
      }
      else if($scope.filterValPark!=undefined){
        $scope.parkeddata=[];
        $scope.parkeddata=$scope.timeFilter_park($scope.filterLoc, $scope.filterValPark); 
      }else if($scope.filterValStop!=undefined){
         $scope.stopReport=[];
         $scope.stopReport=$scope.timeFilter_stop($scope.filterLoc,$scope.filterValStop);
      }

      $scope.alertMe_click($scope.downloadid);
  };
    

    $scope.recursive   = function(location_over,index){
      var indexs = 0;
      angular.forEach(location_over, function(value, primaryKey){
        indexs = primaryKey;
        if(location_over[indexs].address == undefined)
        {
          //console.log(' address over speed'+indexs)
          var latOv    =  location_over[indexs].latitude;
          var lonOv    =  location_over[indexs].longitude;
          var tempurlOv  =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latOv+','+lonOv+"&sensor=true";
          //console.log(' in overspeed '+indexs)
          delayed(3000, function (indexs) {
                return function () {
                  google_api_call_Over(tempurlOv, indexs, latOv, lonOv);
                };
              }(indexs));
        }
      })
  }

  function google_api_call_Over(tempurlOv, indexs, latOv, lonOv){
    $http.get(tempurlOv).success(function(data){
      $scope.oaddress[indexs] = data.results[0].formatted_address;
      // var t = vamo_sysservice.geocodeToserver(latOv,lonOv,data.results[0].formatted_address);
    })
  }


  function google_api_call(tempurlMo, index1, latMo, lonMo) {
    $http.get(tempurlMo).success(function(data){
      $scope.maddress1[index1] = data.results[0].formatted_address;
      // var t = vamo_sysservice.geocodeToserver(latMo,lonMo,data.results[0].formatted_address);
    })
  };
  $scope.recursive1   =   function(locations, indes)
  {
    var index1  =  0;
    angular.forEach(locations, function(value, primaryKey){
        index1 = primaryKey;
        if(locations[index1].address == undefined)
        {
          //console.log(' address movementreport'+index1)
          var latMo    =  locations[index1].latitude;
          var lonMo    =  locations[index1].longitude;
          var tempurlMo  =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latMo+','+lonMo+"&sensor=true";
          //console.log('  movement report  '+index1)
          delayed1(3000, function (index1) {
                return function () {
                  google_api_call(tempurlMo, index1, latMo, lonMo);
                };
              }(index1));
        }
      })
  }

  var delayed = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
  }());
  var delayed1 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
  }());
  var delayed2 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
  }());
  var delayed3 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
  }());
  var delayed4 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
  }());

  var delayed5 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
  }());

  var delayed6 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
  }());

  $scope.address_click = function(data, ind)
  {
    var urlAddress    = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+data.latitude+','+data.longitude+"&sensor=true"
    $http.get(urlAddress).success(function(response)
    {
      data.address  = response.results[0].formatted_address;
      // var t      =   vamo_sysservice.geocodeToserver(data.latitude,data.longitude,response.results[0].formatted_address);
    });
  }


  function google_api_call_stop(tempurlStop, index2, latStop, lonStop) {
    $http.get(tempurlStop).success(function(data){
      $scope.saddressStop[index2] = data.results[0].formatted_address;
      // var t = vamo_sysservice.geocodeToserver(latStop,lonStop,data.results[0].formatted_address);
    })
  };

  $scope.recursiveStop   = function(locationStop,indexStop){
      var index2 = 0;
      angular.forEach(locationStop, function(value, primaryKey){
        index2 = primaryKey;
        if(locationStop[index2].address == undefined)
        {
          //console.log(' address stop'+index2)
          var latStop    =  locationStop[index2].latitude;
          var lonStop    =  locationStop[index2].longitude;
          var tempurlStop  =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latStop+','+lonStop+"&sensor=true";
          //console.log('  stopped or parked '+index2)
          delayed2(3000, function (index2) {
                return function () {
                  google_api_call_stop(tempurlStop, index2, latStop, lonStop);
                };
              }(index2));
        }
      })
  }
  function google_api_call_Idle(tempurlIdle, index3, latIdle, lonIdle) {
    $http.get(tempurlIdle).success(function(data){
      $scope.addressIdle[index3] = data.results[0].formatted_address;
      // var t = vamo_sysservice.geocodeToserver(latIdle,lonIdle,data.results[0].formatted_address);
    })
  };
  function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
    $http.get(tempurlEvent).success(function(data){
      $scope.addressEvent[index4] = data.results[0].formatted_address;
      console.log(' address '+$scope.addressEvent[index4])
      // var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
    })
  };
  $scope.recursiveIdle   = function(locationIdle,indexIdle){
    var index3 = 0;
    angular.forEach(locationIdle, function(value, primaryKey){
        index3 = primaryKey;
        if(locationIdle[index3].address == undefined)
        {
          //console.log(' address idle'+index3)
          var latIdle    =  locationIdle[index3].latitude;
          var lonIdle    =  locationIdle[index3].longitude;
          var tempurlIdle  =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latIdle+','+lonIdle+"&sensor=true";
          delayed3(3000, function (index3) {
                return function () {
                  google_api_call_Idle(tempurlIdle, index3, latIdle, lonIdle);
                };
              }(index3));
        }
      })
  }


  $scope.recursiveEvent   =   function(locationEvent, indexEvent)
  {
    var index4 = 0;
    angular.forEach(locationEvent, function(value ,primaryKey){
      //console.log(' primaryKey '+primaryKey)
      index4 = primaryKey;
      if(locationEvent[index4].address == undefined)
      {
        var latEvent     =  locationEvent[index4].latitude;
        var lonEvent     =  locationEvent[index4].longitude;
        var tempurlEvent =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";
        delayed4(2000, function (index4) {
              return function () {
                google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
              };
            }(index4));
      }
    })
  }

  function google_api_call_Load(tempurlLoad, index5, latLoad, lonLoad) {
    $http.get(tempurlLoad).success(function(data){
      $scope.addressLoad[index5] = data.results[0].formatted_address;
      
    })
  };

  $scope.recursiveLoad  =   function(locationLoad, indexLoad)
  {
    var index5 = 0;
    angular.forEach(locationLoad, function(value ,primaryKey){
      //console.log(' primaryKey '+primaryKey)
      index5 = primaryKey;
      if(locationLoad[index5].address == undefined)
      {
        var latLoad    =  locationLoad[index5].latitude;
        var lonLoad    =  locationLoad[index5].longitude;
        var tempurlLoad = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latLoad+','+lonLoad+"&sensor=true";
        delayed5(2000, function (index5) {
              return function () {
                google_api_call_Load(tempurlLoad, index5, latLoad, lonLoad);
              };
            }(index5));
      }
    })
  }

  function google_api_call_Fuel(tempurlFuel, index6, latFuel, lonFuel) {
    $http.get(tempurlFuel).success(function(data){
      $scope.addressFuel[index6] = data.results[0].formatted_address;
    })
  };

  $scope.recursiveFuel  =   function(locationFuel, indexFuel)
  {
    var index6 = 0;
    angular.forEach(locationFuel, function(value ,primaryKey){
      //console.log(' primaryKey '+primaryKey)
      index6 = primaryKey;
      if(locationFuel[index6].address == undefined)
      {
        var latFuel    =  locationFuel[index6].latitude;
        var lonFuel    =  locationFuel[index6].longitude;
        var tempurlFuel = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latFuel+','+lonFuel+"&sensor=true";
        delayed6(2000, function (index6) {
              return function () {
                google_api_call_Fuel(tempurlFuel, index6, latFuel, lonFuel);
              };
            }(index6));
      }
    })
  }

  
  $scope.getParkedCorrectHours  = function(data) {
      return $scope.msToTime(data);
    }
  
  function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }
  
  function convert_to_24h(time_str) {
    //console.log(time_str);
    var str   = time_str.split(' ');
    var stradd  = str[0].concat(":00");
    var strAMPM = stradd.concat(' '+str[1]);
    var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
      var hours = Number(time[1]);
      var minutes = Number(time[2]);
      var seconds = Number(time[3]);
      var meridian = time[4].toLowerCase();
  
      if (meridian == 'p' && hours < 12) {
        hours = hours + 12;
      } 
      else if (meridian == 'a' && hours == 12) {
        hours = hours - 12;
      }     
      var marktimestr = ''+hours+':'+minutes+':'+seconds;   

      //alert(marktimestr);

      return marktimestr;
    };
  
  $scope.genericFunction = function(vehid, index, shortname,position, address) {
        $scope.vehiname=vehid;
        if(position === "N" || position === 'Z') {
           alert(address);
        } else {
           sessionValue(vehid, $scope.gName);
           var pageUrl='history?vid='+vehid+'&vg='+$scope.gName+'&tn='+$scope.tn;
           console.log(pageUrl);
           $(location).attr('href',pageUrl);
        }

  }

  $scope.groupSelection = function(groupname, groupid){

    $scope.gIndex  =  groupid;
   //startLoading();
    var urlGroup = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupname;
    $http.get(urlGroup).success(function(data){
      $scope.vehiname   =  data[groupid].vehicleLocations[0].vehicleId;
      $scope.shortNam   =  data[groupid].vehicleLocations[0].shortName;
      $scope.gName      =  data[groupid].group; 
    //$scope.locations  =  data;
      $scope.vehicle_list =  data;
      sessionValue($scope.vehiname, $scope.gName);
      var pageUrl='history?vid='+$scope.vehiname+'&vg='+$scope.gName+'';
      $(location).attr('href',pageUrl);
   //stopLoading();
   });

  }
  
  $scope.getLocation  = function(lat,lon,ind) { 
    //alert(ind);
    switch($scope.downloadid) {
      case 'overspeedreport':
        $scope.recursive($scope.overspeeddata,ind);
      break;
      case 'movementreport':
        $scope.recursive($scope.movementdata,ind);
      break;
      case 'stoppedparkingreport':
        $scope.recursive($scope.parkeddata,ind);
      break;
      default:
      break;
    }
  };
  
  $scope.alertMe    = function(data) {  
   // console.log(data);
    switch(data) {
      case 'All':
        $scope.downloadid  =  'allreport';
        $scope.tn='all';
        //$scope.recursive1($scope.allData,0);
      break;
      case 'noData':
        $scope.tn='noData';
        $scope.downloadid  =  'nodatareport';
        //$scope.recursive1($scope.noData,0);
      break;
      case 'Overspeed':
        $scope.tn='overspeed';
        $scope.downloadid  =  'overspeedreport';
        $scope.overallEnable =  true;
        $scope.recursive($scope.overspeeddata,0);
      break;
      case 'Movement':
        $scope.tn='movement';
        $scope.downloadid  =  'movementreport';
        $scope.overallEnable =  true;
        //clearTimeout(promis);
        $scope.recursive1($scope.movementdata,0);
      break;
      case 'Stopped/Parked':
        $scope.tn='parked';
        $scope.downloadid  =  'stoppedparkingreport';
        $scope.overallEnable =  true;
        $scope.recursiveStop($scope.parkeddata,0);
      break;
      case 'Geo Fence':
        $scope.downloadid  =  'geofencereport';
        $scope.overallEnable =  false;
      break;
      case 'idlereport':
        $scope.tn='idle';
        $scope.downloadid    =  'idlereport';
        $scope.overallEnable =  true;
        $scope.recursiveIdle($scope.idlereport,0);
      break;
      case 'eventReport':

        $scope.downloadid    =  'eventReport';
        $scope.overallEnable =  true;
        $scope.recursiveEvent($scope.eventReportData,0);
      break;
      case 'sitereport':
        $scope.overallEnable =  true;
        $scope.downloadid    =  'sitereport';
      break;
      case 'loadreport':
        $scope.downloadid    =  'loadreport';
        $scope.overallEnable =  true;
      //$scope.recursiveLoad($scope.loadreport,0);
      break;
      case 'fuelreport':
        $scope.downloadid    =  'fuelreport';
        $scope.overallEnable =  true;
        $scope.fuelChart($scope.fuelValue);
        $scope.recursiveFuel($scope.fuelValue, 0);
      break;
      case 'ignitionreport':
        $scope.tn='ignition';
        $scope.downloadid   = 'ignitionreport';
      break;
    /*case 'acreport':
        $scope.downloadid   = 'acreport';
      break; */
      case 'stoppageReport':
        $scope.tn='stoppageReport';
        $scope.downloadid   = 'stoppageReport';
      break;
      default:
      break;
    }
  };

  $scope.ovrSpeedGraph  =   function(data) {

    var grpDate   = [];
    var ovrSpd    = [];
  //console.log(data);

    try {

      if(data.length) {

        for (var i = 0; i < data.length; i++) {

        //if(data[i].overSpeedTime > 0 ) {
            if( data[i].speed >= 0 ) {
              ovrSpd.push(data[i].speed);
               var dar = $filter('date')(data[i].date, "dd/MM/yyyy HH:mm:ss");
                grpDate.push(dar);
            }
        };
      }

    } catch (err) {
      console.log(err.message);
    }

     $(function () {
   
        $('#container3').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: translate('Speed')
            },
            credits: {
              enabled: false
            },
            xAxis: {
               categories: grpDate,
                 title: {
                    text: translate('Date & Time')
                 }

            },
            
            yAxis: {
                title: {
                    text: translate('Speed(Kms)')
                },
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'Speed',
                data:  ovrSpd
            }]
        });

      });

  

  }

 
  $scope.fuelChart = function(data) {

    var ltrs         =  [];
    var fuelDate     =  [];
    var distCovered  =  [];
    var spdVals      =  [];
    var tankSize     =  parseFloat($scope.tankSize);
  //alert(tankSize);

    console.log(data);

    try {
      if(data.length) {
        for (var i = 0; i < data.length; i++) {
          if(data[i].fuelLitre !='0' || data[i].fuelLitre !='0.0') {
          //ltrs.push(data[i].fuelLitre);
            ltrs.push( { y:parseFloat(data[i].fuelLitre), odo:data[i].odoDistance, speed:data[i].speed, ignition:data[i].ignitionStatus } ); 
            var dar = $filter('date')(data[i].date, "dd/MM/yyyy HH:mm:ss");
            fuelDate.push(dar);
            distCovered.push(data[i].distanceCovered);
            spdVals.push(data[i].speed);
          }

        };
      }
    } catch (err) {
      console.log(err.message)
    }

    //console.log(ltrs);
    
  $(function () {
   
        $('#container').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: translate('Fuel')
            },
            credits: {
              enabled: false
            },
            xAxis: {
               categories: fuelDate,
                 title: {
                    text: translate('Date&Time')
                 }
            },           
            yAxis: {
              title: {
                text: translate('Fuel(Ltrs)')
              },
              max: tankSize,
              min: 0,
              endOnTick: false 
            },
            tooltip: {
              formatter: function () {
                var s,a=0;
              //console.log(this.points);

                $.each(this.points, function () {
                        //a+=1;   
                        //if(a==2){ 
                            s =  '<b>'+this.x+'</b>'+'</b>'+'<br>'+' '+'</br>'+
                                 '--------------------'+'<br>'+' '+'</br>'+
                                 translate('Fuel')+'     : ' +'  '+'<b>' + this.point.y + '</b>'+' Ltrs'+'<br>'+' '+'</br>'+
                                 translate('Speed')+'    : ' +'  '+'<b>' + this.point.speed + '</b>'+' Kmph'+'<br>'+' '+'</br>'+
                                 translate('Ignition')+' : ' +'  '+'<b>' + this.point.ignition + '</b>'+'<br>'+' '+'</br>'+
                                 translate('Odo')+'      : ' +'  '+'<b>' + this.point.odo + '</b>';
                         //}       
                });
             return s;
            },
            shared: true
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null,
                    turboThreshold: 0
                }
            },

            series: [{
                type: 'area',
                name: 'Fuel Level',
                data: ltrs
            }]
        });


        $('#container2').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: translate('FuelDistance')
            },
            credits: {
               enabled: false
            },
            xAxis: {
              categories: distCovered,
              title: {
                    text: translate('Distance(Kms)')
              }
            },
            
            yAxis: {
                title: {
                    text: translate('Fuel(Ltrs)')
                },
                max: tankSize,
                min: 0,
                endOnTick: false 
            },
            tooltip: {
                formatter: function () {
                    var s;
                    //console.log(this.points);
                      $.each(this.points, function () {                    
                          s  =  translate('Fuel')+'      : ' +'  '+'<b>'+this.y + '</b>'+' Ltrs'+'<br>'+' '+'</br>'+
                                translate('Distance')+'  : ' +'  '+'<b>'+this.x + '</b>'+' Kms'+'<br>';
                      });

                 return s;
                },
                shared: true
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null,
                    turboThreshold: 0
                }
            },

            series: [{
                type: 'area',
                name: 'Fuel Level',
                data: ltrs
            }]
        }); 


          Highcharts.chart('container4', {
              chart: {
                  width:900,
                  height: 300,
                  type: 'spline',
                  zoomType: 'xy'              
              },
              title: {
                  text: translate('FuelSpeed')
              },
              legend: {
               /* layout: 'vertical',
                  align: 'left',
                  verticalAlign: 'top',
                  x: 150,
                  y: 100,*/
                  floating: true,
               /* borderWidth: 1,
                  backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'*/
              },
              xAxis: {
                  categories: fuelDate,
                  plotBands: [{ // visualize the weekend
                      from: 4.5,
                      to: 6.5,
                      color: 'rgba(68, 170, 213, .2)'
                  }]
              },
              yAxis: {
                  title: {
                      text: translate('Speed&Fuel')
                  },
                max: tankSize,
                min: 0,
                endOnTick: false 
              },
              tooltip: {
                  shared: true,
                  valueSuffix: 'Kmph/Ltrs'
              },
              credits: {
                  enabled: false
              },
              plotOptions: {
                  areaspline: {
                      fillOpacity: 0.5
                  }
              },
              series: [{
                  name: translate('Speed'),
                  data: spdVals
              }, {
                  name: translate('Fuel'),
                  data: ltrs
              }]
          });

   });

 }


  $scope.exportData = function (data) {
    //console.log(data);
    var blob = new Blob([document.getElementById(data).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
    };
    
    $scope.exportDataCSV = function (data) {
    //console.log(data);
    CSV.begin('#'+data).download(data+'.csv').go();
    };
    
    $scope.msToTime   = function(ms) {
      days       = Math.floor(ms / (24*60*60*1000));
      daysms     = ms % (24*60*60*1000);
      hours      = Math.floor((ms)/(60*60*1000));
      hoursms    = ms % (60*60*1000);
      minutes    = Math.floor((hoursms)/(60*1000));
      minutesms  = ms % (60*1000);
      sec = Math.floor((minutesms)/(1000));
      // return days+"d : "+hours+"h : "+minutes+"m : "+sec+"s";
      if(hours=="NaN"||hours==Infinity)hours=0;
      if(minutes=="NaN"||minutes==Infinity)minutes=0;
      if(sec=="NaN"||sec==Infinity)sec=0;
      return hours+":"+minutes+":"+sec;
    }

     $scope.msToTime2   = function(ms) {
      days = Math.floor(ms / (24*60*60*1000));
      daysms=ms % (24*60*60*1000);
      hours = Math.floor((ms)/(60*60*1000));
      hoursms=ms % (60*60*1000);
      minutes = Math.floor((hoursms)/(60*1000));
      minutesms=ms % (60*1000);
      sec = Math.floor((minutesms)/(1000));

      return hours+"h : "+minutes+"m : "+sec+"s";

    /*  if(days>0) {
        return days+"d : "+hours+"h : "+minutes+"m : "+sec+"s";
      } else {
        return hours+"h : "+minutes+"m : "+sec+"s";
      }*/
      //return hours+":"+minutes+":"+sec;
    }


    //submit button click function
    $scope.buttonClick;
  $scope.plotHist     = function() {
    $scope.ignDurTot      =  "0h : 0m : 0s"; 
    if((checkXssProtection($scope.fromdate) == true) && (checkXssProtection($scope.todate) == true) && (checkXssProtection($scope.fromtime) == true) && (checkXssProtection($scope.totime) == true))
    {
      startLoading();
        var valueas   = $('#txtv').val();

         $scope.fromdate  =  $('#dateFrom').val();
         $scope.todate    =  $('#dateTo').val();
         $scope.fromtime  =  $('#timeFrom').val();
         $scope.totime    =  $('#timeTo').val();
         if(localStorage.getItem('timeTochange')!='yes'){
             updateToTime();
             $scope.totime    =   localStorage.getItem('toTime');
         }
         $scope.toTimeVal = utcFormat($scope.todate,convert_to_24h($scope.totime));

        // alert($scope.fromdate);
        // alert($scope.todate);
        // alert( convert_to_24h($scope.fromtime) );
        // alert( convert_to_24h($scope.totime) );

        var histurl   = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromdate,convert_to_24h($scope.fromtime))+'&toDateUTC='+utcFormat($scope.todate,convert_to_24h($scope.totime));
     // var histurl   = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+'&fromDateUTC='+utcFormat($scope.fromdate,convert_to_24h($scope.fromtime))+'&toDateUTC='+utcFormat($scope.todate,convert_to_24h($scope.totime));
     // var histurl   = "http://"+getIP+context+"/public//getVehicleHistory?vehicleId="+prodId+"&fromDate="+$scope.fromdate+"&fromTime="+convert_to_24h($scope.fromtime)+"&toDate="+$scope.todate+"&toTime="+convert_to_24h($scope.totime)+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromdate,convert_to_24h($scope.fromtime))+'&toDateUTC='+utcFormat($scope.todate,convert_to_24h($scope.totime));
     // var loadUrl   = "http://"+getIP+context+"/public//getLoadReport?vehicleId="+prodId+"&fromDate="+$scope.fromdate+"&fromTime="+convert_to_24h($scope.fromtime)+"&toDate="+$scope.todate+"&toTime="+convert_to_24h($scope.totime);
    try{
      $http.get(histurl).success(function(data){

        $scope.errMsg=data.error;
     // $scope.loading       = false;
        $scope.hist          =  data;
        $scope.showErrMsg    =  true;
        $scope.cardData      =  true;
        $scope.filterLoc     =  data.vehicleLocations;
        $scope.topspeedtime  =  data.topSpeedTime;
      //$scope.tankSize      =  parseInt(data.tankValue);
        $scope.tankSize      =  data.tankValue;
        // loadReportApi(loadUrl);
        $scope.dataArray_click(data.vehicleLocations);
        // $('#status').fadeOut(); 
        // $('#preloader').delay(350).fadeOut('slow');
        stopLoading();  
      });
    }
    catch (err){
      console.log(' err '+err);
      // $('#status').fadeOut(); 
      //  $('#preloader').delay(350).fadeOut('slow');
      stopLoading();  
     }
    }
  }
     
  //pdf method
  $scope.pdfHist     =   function() {  
       
       var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+$scope.vvid+"&fromDate="+$scope.fd+"&fromTime="+convert_to_24h($scope.ft)+"&toDate="+$scope.td+"&toTime="+convert_to_24h($scope.tt)+"&interval="+$scope.interval;      
    // var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+$scope.vvid+"&fromDate="+$scope.fd+"&fromTime="+convert_to_24h($scope.ft)+"&toDate="+$scope.td+"&toTime="+convert_to_24h($scope.tt);      
    // console.log(histurl);   

    //var toDateTimeVal = utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));

    $http.get(histurl).success(function(data){

      $scope.errMsg      =  data.error;
      $scope.hist        =  data;
      $scope.showErrMsg  =  true;
      $scope.cardData    =  true;
      $scope.filterLoc   =  data.vehicleLocations;
    //$scope.tankSize    =  parseInt(data.tankValue);
      $scope.tankSize    =  data.tankValue;

      $scope.dataArray(data.vehicleLocations);

      switch($scope.repId) {
        case 'movementreport':
          $scope.recursive1($scope.movementdata,0);
          break;
        case 'stoppedparkingreport':
          $scope.recursiveStop($scope.parkeddata,0);
          break;
        case 'idlereport':
          $scope.recursiveIdle($scope.idlereport,0);
          break; 
        default:
          break;
      }     
    });
    
  }
 
  function dateStringFormat(d) {
    var s     =     d.split(' ');
    var t     =     s[0].split('-');
    var ds    =     (t[2].concat('-'+t[1]).concat('-'+t[0])).concat(' '+s[1]);
    return new Date(ds).getTime();
  }
  
   $scope.figureOutTodosToDisplay = function() {
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.filteredTodos = $scope.movementdata.slice(begin, end);
    };
    
      
    $scope.pageChanged = function() {
      $scope.figureOutTodosToDisplay();
    };
  
}]);
// app.factory('vamo_sysservice', function($http, $q){
//  return {
//    geocodeToserver: function (lat, lng, address) {
//      try { 
//        var reversegeourl = GLOBAL.DOMAIN_NAME+'/store?geoLocation='+lat+','+lng+'&geoAddress='+address;
//          return this.getDataCall(reversegeourl);
//      }
//      catch(err){ console.log(err); }
      
//    },
//         getDataCall: function(url){
//          var defdata = $q.defer();
//          $http.get(url).success(function(data){
//               defdata.resolve(data);
//      }).error(function() {
//                     defdata.reject("Failed to get data");
//             });
//      return defdata.promise;
//         }
//     }
// });

app.directive("getLocation", function () {
  return {
    restrict: "A",
    replace: true,    
    link: function (scope, element, attrs) {
      angular.element(element).on('click', function(){    //mouseenter
        var lat = attrs.lat;
        var lon = attrs.lon;
        var ind = attrs.index;
        console.log(ind);
      scope.getLocation(lat,lon,ind);
    });
    }
  };
});
