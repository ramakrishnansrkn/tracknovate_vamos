<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>

<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

body{
font-family: 'Lato', sans-serif;
/*font-weight: bold;*/  

/*font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
} 

</style>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
       <?php include('sidebarList.php');?>
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>

    <div class="col-md-12">

        <div class="box box-primary" style="height: 205px;">

                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title"><?php echo Lang::get('content.employee_attendance'); ?></h3>
                   
                </div>
              

                        <div class="row"  style="margin-top: 10px;margin-left: 215px;">
                              <div class="col-md-1" align="center"></div>
                              <div class="col-md-2 form-group" style="right:0px;padding-top: 7px;"><?php echo Lang::get('content.month_year'); ?> :</div>
                              <div class="input-group datecomp" style="padding-bottom: 20px;">
                                            <input type="text" ng-model="fromMonthss" ng-change="submitMon()" class="form-control placholdercolor" id="monthFrom" placeholder="Month">
                              </div>
                              
                        </div>

                        <div class="row"  style="margin-top: 10px;margin-left: 215px;">
                                <div class="col-md-1" align="center">
                                     <button style="margin-top: 20px;margin-left: 190px; padding : 3px;position: absolute;" ng-click="submitMon()"><?php echo Lang::get('content.submit'); ?></button>
                                </div>
                        </div>
                </div>
    </div>


        <div class="col-md-12">
            <div class="box box-primary" style="min-height:570px;">
        
                <div>

                    <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">
                     
                        <img style="cursor: pointer;" ng-click="exportData('EmployeeAtnReport')"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('EmployeeAtnReport')"  src="../app/views/reports/image/csv.jpeg" />
                    </div>

              <div class="box-body" id="EmployeeAtnReport">
                <div class="empty" align="center"></div>
           
                  <div class="row" style="padding-top: 20px;"></div>

                          <table class="table table-bordered table-striped table-condensed table-hover table-striped" ng-show="showMonTable">

                                <thead>
                                     <tr style="height:35px;text-align: center;">
                                       <td colspan="36" style="font-size:14px;font-weight: bold;background-color:#C2D2F2;">{{monValFront+curYearFront}}</td>
                                     </tr>

                                     <tr>
                                         <td> <?php echo Lang::get('content.veh_name'); ?> </td>
                                         <td> <?php echo Lang::get('content.designation'); ?> </td>
                                         <td> <?php echo Lang::get('content.name'); ?> </td>
                                         <td ng-repeat="dat in monthDates" style="background-color:#ecf7fb;font-weight: bold;">{{dat}}</td>
                                         <td> <?php echo Lang::get('content.total_pre'); ?></td>
                                         <td> <?php echo Lang::get('content.total_abs'); ?></td>
                                      </tr>
                                </thead>

                                <tbody ng-repeat="value in empData track by $index">

                                     <tr>
                                        <td ng-if="value.attendDriver.length!=0 && $odd" rowspan="2" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;">{{value.vehiNam}}</td>
                                        <td ng-if="value.attendDriver.length!=0 && $even" rowspan="2" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;">{{value.vehiNam}}</td>
                                        <td ng-repeat="val in value.attendDriver track by $index" style="color:#557fa8;">{{val.atnVal}}</td>
                                     </tr>

                                     <tr>
                                        <td ng-if="value.attendHelper.length!=0" ng-repeat="val in value.attendHelper track by $index" style="color:#e18e32;">{{val.atnVal}}</td>
                                     </tr>
                                     <tr ng-if="conEmpError" style="text-align: center">
                                    <td colspan="24" class="err"><h5>{{conEmpError}}</h5></td>
                                  </tr>

                                </tbody> 

                           <!-- <tr ng-if="monthFuelData.cumulativeVehicleDistanceData!=null">
                                    <td rowspan="2" style="background-color:#ecf7fb;padding-top: 15px;">Total (Kms/Ltrs)</td>
                                    <td>Dist</td>
                                    <td ng-repeat="val in totMonDistVehic track by $index" style="color:#557fa8;font-weight: bold;">{{val}}</td>
                                </tr>
                                <tr ng-if="monthFuelData.cumulativeVehicleFuelData!=null">  
                                    <td>Fuel</td>
                                    <td ng-repeat="val in totMonFuelVehic track by $index" style="color:#e18e32;font-weight: bold;">{{val}}</td>
                                </tr>
                                <tr ng-if="monthFuelData.error != '' " align="center">
                                    <td colspan="36" class="err"><h5>Please choose current month (or) before</h5></td>
                                </tr> -->
                                 
                            </table>

                        </div>
                    </div>
                </div>
            </div>

          </div>
        </div>


    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script>-->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/empAtnReport.js"></script>
    
    <script>

      $("#example1").dataTable();
          
      $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
      });
        
      $(function () {

                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false,
                    maxDate: new Date,
                    minDate: new Date(2015, 12, 1)
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false
                });
                $('#timeTo').datetimepicker({
                    useCurrent:true,
                    pickDate: false
                });
             

                $('#monthFrom').datetimepicker({
                    minViewMode: 'months',
                    viewMode: 'months',
                    pickTime: false,
                    useCurrent:true,
                    format:'MM/YYYY',
                    maxDate: new Date,
                    minDate: new Date(2015, 12, 1)
                });
      }); 

        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

  </script>
    
</body>
</html>
