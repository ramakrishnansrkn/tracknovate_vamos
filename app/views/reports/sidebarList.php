<div id="sidebar-wrapper">

            <ul class="sidebar-nav">
                <li class="sidebar-brand"><a href="javascript:void(0);"><img id="imagesrc" src=""/></i></a></li>
                <li class="track"><a href="../public/track"><div></div><label><?php echo Lang::get('content.track'); ?></label></a></li>
                <!-- <li class="history"><a href="../public/track?maps=replay"><div></div><label>History</label></a></li> -->
                <li class="alert01" ng-if="!reports"><a href="../public/reports" ><div></div><label><?php echo Lang::get('content.reports'); ?></label></a></li>
                <li class="alert01" ng-if="reports"><a href="javascript:void(0);" class="active"><div></div><label><?php echo Lang::get('content.reports'); ?></label></a></li>
                <li class="stastics" ng-if="!stastics"><a href="../public/statistics?ind=1"><div></div><label><?php echo Lang::get('content.statistics'); ?></label></a></li>
                <li class="stastics" ng-if="stastics"><a href="javascript:void(0);" class="active"><div></div><label><?php echo Lang::get('content.statistics'); ?></label></a></li>
                <li class="admin"><a href="../public/settings"><div></div><label><?php echo Lang::get('content.scheduled'); ?></label></a></li>
                <li class="fms" ng-if="!fmsactive"><a href="../public/fleetManagement?fms=FleetManagement"><div></div><label><?php echo Lang::get('content.FMS'); ?></label></a></li>
                <li class="fms" ng-if="fmsactive"><a href="../public/fleetManagement?fms=FleetManagement" class="active"><div></div><label><?php echo Lang::get('content.FMS'); ?></label></a></li>
                <li><a href="../public/logout"><img src="assets/imgs/logout.png"/></a></li>
            </ul>
            
            <ul class="sidebar-subnav" style="overflow-y: auto;">
              <!-- <li style="padding-left:25px;">
                 <div class="right-inner-addon" align="center"> 
                      <select ng-model="multiLang" id="lang" class="form-control" >
                            <option value="en">English</option>
                            <option value="hi">Hindi</option>
                     </select>
                </div>
               </li> -->
                <li style="padding-left:25px;">
                    <div class="right-inner-addon" align="center"> 
                    <i class="fa fa-search"></i>
                    <input type="search" class="form-control" placeholder="Search" ng-model="searchbox" name="search" />
                    </div>
                </li>

      <li ng-repeat="location in vehicle_list track by $index | orderBy:natural('group')" class="active"><a href="javascript:void(0);" ng-click="groupSelection(location.group, location.rowId)">{{trimColon(location.group)}}</a>
        <ul class="nav" style="max-height:400px;overflow-x:hidden;overflow-y:scroll;">
            <li ng-repeat="loc in location.vehicleLocations | orderBy:natural('shortName') | filter:searchbox" data-trigger="hover" ng-class="{active:vehiname ==loc.vehicleId}">
              <a href="javascript:void(0);"  ng-class="{red:loc.status=='OFF'}" ng-click="genericFunction(loc.vehicleId, $index,loc.shortName,loc.position, loc.address,loc.groupName)" ng-cloak>
              <img ng-if="trvShow=='true'" ng-src="assets/imgs/trvSideMarker/{{loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/>
              <img ng-if="trvShow!='true'" ng-hide="vehiImage" ng-src="assets/imgs/sideMarker/{{loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" 
              width="16" height="16"/>
              <img ng-if="trvShow!='true'" ng-show="vehiImage" ng-src="assets/imgs/assetImage.png" fall-back-src="assets/imgs/assetImage.png" width="16" height="16"/>
                        <span  tooltips tooltips-position="right"> {{loc.shortName}} </span>
                      </a> 
                   </li>
                </ul>
            </li>
           </ul>
            
</div>