<!DOCTYPE html>
<html lang="en">
<head>
  <title>Password Reset</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="../../vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="../../fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<link rel="stylesheet" type="text/css" href="../../vendor/animate/animate.css">
<link rel="stylesheet" type="text/css" href="../../vendor/css-hamburgers/hamburgers.min.css">
<link rel="stylesheet" type="text/css" href="../../vendor/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="../../css/util.css">
<link rel="stylesheet" type="text/css" href="../../css/main.css">
</head>
<body>
  
  <div class="limiter">
    <div class="container-login100" style="background-image: url('../../assets/imgs/bg.jpg'); ">
      <div class="wrap-login100 p-t-190 p-b-30" style="margin-top: -10%;">
        <form class="login100-form validate-form" action="../../password/update" novalidate>
        @if (Session::has('error'))
            {{ trans(Session::get('reason')) }}
          @endif
          <div class="login100-form-avatar" style="width: 55%; height: 120px; border-radius: 0%;">
            <img src="../../uploads/<?php echo $web ?>.png" style="width: 98%;">
          </div>

          <span class="login100-form-title p-t-20 p-b-45" style="font-family: Arial, Helvetica, sans-serif;">
          GPS Tracking System
           <span style="color:#ff6666;font-weight:bold;font-size:15px; text-align: center;">{{ HTML::ul($errors->all()) }}</span> </span>  
          <div class="wrap-input100 validate-input m-b-10" >
            <input class="input100" type="text" name="userId" value="<?php echo $userId ?>" readOnly="true" style="background-color: #cccac0a8;">
            {{ Form::hidden('token', $token) }}
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-user"></i>
            </span>
          </div>
          <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
            <input class="input100" type="password" name="password" placeholder="New Password" onmouseover="mouseoverPass()" onmouseout="mouseoutPass()" id="pass" >
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock"></i>
            </span>
          </div>

          <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
            <input class="input100" type="password" name="password_confirmation" placeholder="Re-type Your New Password"  onmouseover="mouseoverPass1()" onmouseout="mouseoutPass1()" id="pass1" >
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock"></i>
            </span>
          </div>

          <div class="container-login100-form-btn p-t-10">
            <button class="login100-form-btn">
              Reset
            </button>
          </div>

        </form>
      </div>
    </div>
  </div>
  
  
<script src="../../vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="../../vendor/bootstrap/js/popper.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="../../vendor/select2/select2.min.js"></script>
<script src="../../js/main.js"></script>
<script type="text/javascript">
function mouseoverPass(obj) {
  var obj = document.getElementById('pass');
  obj.type = "text";
}
function mouseoutPass(obj) {
  var obj = document.getElementById('pass');
  obj.type = "password";
}
function mouseoverPass1(obj) {
  var obj = document.getElementById('pass1');
  obj.type = "text";
}
function mouseoutPass1(obj) {
  var obj = document.getElementById('pass1');
  obj.type = "password";
}
</script>

</body>
</html>
