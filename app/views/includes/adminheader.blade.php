<!-- app/views/nerds/index.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<title>VAMO Systems</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
	<style type="text/css">
		.navbar {
   			 border: 1px solid #080808;
		}
		.nav > li > a:hover{
		    background-color:#005db4;
		}
		.bg-primary {
 		   color: #fff;
   		 background-color: #1476ca;
		}
	</style>
</head>
<body>
	
<div class="container">
<div>
<nav class="navbar navbar-dark bg-primary" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('vdmFranchises') }}" style="color: white;
">VAMO SYSTEMS</a>
		

	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('vdmFranchises/fransearch') }}" style="color: white;
">Switch Login</a></li>
		<li><a href="{{ URL::to('vdmFranchises/users') }}" style="color: white;
">Switch Login users</a></li>
		<li><a href="{{ URL::to('vdmFranchises') }}" style="color: white;
">View All Franchises</a></li>
		<li><a href="{{ URL::to('vdmFranchises/create') }}" style="color: white;
">Add a Franchise</a>
		<li><a href="{{ URL::to('ipAddressManager') }}" style="color: white;
">IPAddressManager</a>
		<li><a href="{{ URL::to('vdmFranchises/buyAddress') }}" style="color: white;
">Address control</a>
 <li><a href="{{ URL::to('vdmFranchises/fransOnboard') }}" style="color: white;
">Frans Onboard</a></li>
<!-- <li><a href="{{ URL::to('audit','AuditFrans') }}" style="color: white;
">Audit Trial - Franchises</a></li> -->


   <!-- for audit -->
		<!-- <li style="padding-top: 10px;">
		   <div class="dropdown">
		  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Audit
		  <span class="caret"></span></button>
		  <ul class="dropdown-menu">
		    <li ><a href="{{ URL::to('audit','AuditFrans') }}" >Franchies</a></li>
		    <li><a href="{{ URL::to('audit','AuditDealer') }}">Dealer</a></li>
		    <li><a href="{{ URL::to('audit','AuditUser') }}">User</a></li>
		    <li><a href="{{ URL::to('audit','AuditVehicle') }}">Vehicle</a></li>
		    <li><a href="{{ URL::to('audit','RenewalDetails') }}">Renewal Details</a></li>
		  </ul>
		</div>
	 </li>	 -->
	 
<!-- <li><a href="{{ URL::to('vdmFranchises/licenceType/convertion') }}" style="color: white;
">Licence-Type</a></li> -->
		<li><a href="{{ URL::to('logout/') }}" style="color: white;
">Logout User: {{Auth::user ()->username }}</a></li>	
		<!-- <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form> -->

	</ul>

</nav>
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
</div>


@yield('mainContent')

<footer class="row">
		@include('includes.footer')
	</footer>
</div>
</body>
</html>