var getIP = globalIP;

app.controller('mainCtrl',['$scope', '$http', '_global','$filter','$translate',function($scope, $http, GLOBAL,$filter,$translate){
  var language=localStorage.getItem('lang');
  $scope.multiLang=language;
  $translate.use(language);
  var translate = $filter('translate');
  $scope.maps= getParameterByName("maps");
  $scope.map_change=0;
  $scope.csv=false;
  var layer;
  var line;
  var lineList        = [];
  var lineSymbol      = {path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW};
  var text;
  var drop;
  $scope.textValue    = '';
  $scope.dropValue    = '';
  $scope.orgID        = '';
  $scope.orgIdlist    = [];
  $scope.search;
  var seclat          = '';
  var seclan          = '';
  var latlanList      = [];
  var mergeList       = [];
  var dropDown        = [];
  var polygenList     = [];
  var polygenListOSM  = [];
  var polygenColor;
  var polygenShapList = [];
  var shapes_osm  =[];
  var polygon  = [];
  var vehlocUrl           = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
  var editableColor   = {"editable": true, "strokeColor": '#000', "fillColor": "#7cbae8", "fillOpacity": .7, "strokeWeight": 1};
  marker = new google.maps.Marker({});
  var oldName         = '';
  $scope.url          = GLOBAL.DOMAIN_NAME+'/viewSite';
  console.log($scope.url);
  var myOptions       = {
        zoom: 7,
        center: new google.maps.LatLng(25.171717,51.58969167),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControlOptions: {
          position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        streetViewControlOptions: {
          position: google.maps.ControlPosition.LEFT_BOTTOM
        },
  };


  
  var googleMap         = new google.maps.Map(document.getElementById("maploc"), myOptions);
  var polygenValue;
  var shapes         = [];

  var drawingManager = new google.maps.drawing.DrawingManager({
          
          drawingControl: true,
          drawingControlOptions: {
          
            position: google.maps.ControlPosition.LEFT_TOP,
            drawingModes: ['polygon'],
            
          },
          
          polygonOptions: editableColor,
          // circleOptions:editableColor,
          // rectangleOptions:editableColor,

          // markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
          // circleOptions: {
          //   fillColor: '#ffff00',
          //   fillOpacity: 1,
          //   strokeWeight: 5,
          //   clickable: true,
          //   editable: true,
          //   zIndex: 1
          // }
        });

  drawingManager.setMap(googleMap);
  var osmUrl = 'http://159.89.173.199/osm_tiles/{z}/{x}/{y}.png',
      osmAttrib = '&copy; <a href="http://159.89.173.199/nominatim/lf.html">OpenStreeetMap</a> contributors',
      osm = L.tileLayer(osmUrl, {maxZoom: 18, attribution: osmAttrib}),
      mapOSM = new L.Map('map_osm', {layers: [osm], center: new L.LatLng(25.171717,51.58969167), zoom: 7 });
     
        var drawnItems = new L.FeatureGroup();
        mapOSM.addLayer(drawnItems);

        // Set the title to show on the polygon button
        

        var drawControl = new L.Control.Draw({
            position: 'topleft',
            draw: {
                polyline: {
                    metric: true
                },
                polygon: {
                    allowIntersection: false,
                    showArea: true,
                    drawError: {
                        color: '#b00b00',
                        timeout: 1000
                    },
                    shapeOptions: {
                        color: '#333',
                        weight : 1
                    }
                },
                circle: {
                    shapeOptions: {
                        color: '#662d91'
                    }
                },
                marker: true
            },
            edit: {
                featureGroup: drawnItems,
                remove: true
            }
        });
        mapOSM.addControl(drawControl);

        mapOSM.on('draw:created', function (e) {
            var type = e.layerType,
                layer = e.layer;
                shapes_osm=layer;
                _latlngs=e.layer._latlngs;
                listLatLng(_latlngs);
            if (type === 'marker') {
                layer.bindPopup('A popup!');
            }

            drawnItems.addLayer(layer);
            
        });

        mapOSM.on('draw:edited', function (e) {
            var layers = e.layers;
            var countOfEditedLayers = 0;
            layers.eachLayer(function(layer) {
              shapes_osm=layer;
                _latlngs=layer._latlngs;
                countOfEditedLayers++;
            });
            listLatLng(_latlngs);
            console.log("Edited " + countOfEditedLayers + " layers");
        });



         
         function init_map(){
             document.getElementById("pac-input").value = "";
        if($scope.map_change==0) {

               //console.log('init_google.....');
                 document.getElementById("newLeaf").style.display     = "block";
                 document.getElementById("map_osm").style.display="none"; 
                 document.getElementById("maploc").style.display="block"; 
                 
              
                
            } else if($scope.map_change==1) {

                //console.log('init_osm.....');
                  document.getElementById("newLeaf").style.display     = "none";
                  document.getElementById("maploc").style.display="none"; 
                  document.getElementById("map_osm").style.display="block";
                 
                 

                
            }
       }





$scope.changeMap=function(map_no) {

 //console.log('change map...');
   document.getElementById("pac-input").value = "";
   // document.getElementById("inputOsm").value   = "";
    $scope.maps_name=map_no;
    if($scope.maps_name==0){
 
        $scope.map_change = 0;   
        //document.getElementById("selectmap").style.marginTop = "55px";
        document.getElementById("newLeaf").style.display     = "block";
        document.getElementById("map_osm").style.display  = "none"; 
        document.getElementById("maploc").style.display  = "block"; 

         localStorage.setItem('mapNo',0);
         

    } else if($scope.maps_name==1) {

      
          
         $scope.map_change = 1;
         //document.getElementById("selectmap").style.marginTop = "1px";
         document.getElementById("newLeaf").style.display     = "none";
         document.getElementById("maploc").style.display="none"; 
         document.getElementById("map_osm").style.display="block"; 

         localStorage.setItem('mapNo',1);

          
    }
}

  // googleMap         = new google.maps.Map(document.getElementById("maploc"), myOptions);
  //document.getElementById("maploc").style.display="block";
  


  var list  =[];
  $scope.$watch('url', function(){
    init_map();
    serviceCall();
    stopLoading();
  });
  
function listLatLng(_latlng){
  latlanList = [];
  if(_latlng.length > 2){
      angular.forEach(_latlng, function(value, key){
        if($scope.map_change==0)
        latlanList.push(value.lat()+":"+value.lng());
    else
      latlanList.push(value.lat+":"+value.lng);
        //alert(value.lat);
      })
      if($scope.map_change==0)
      latlanList.push(_latlng[0].lat()+":"+_latlng[0].lng());
  else
    latlanList.push(_latlng[0].lat+":"+_latlng[0].lng);
    }
    // console.log(latlanList)
}

google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
  
  shapes.push(polygon);
  listLatLng(polygon.getPath().getArray());

  google.maps.event.addListener(polygon.getPath(), 'set_at', function(po) {
    listLatLng(polygon.getPath().getArray());
    shapes.push(polygon);
  });

  google.maps.event.addListener(polygon.getPath(), 'insert_at', function(po) {
    listLatLng(polygon.getPath().getArray());
    shapes.push(polygon);
  });
  google.maps.event.addListener(polygon.getPath(), 'remove_at', function () {
    listLatLng(polygon.getPath().getArray());
    shapes.push(polygon);
  });

});

  $('#add').hide();
  $("#update").hide();
  $scope.AddClear = function(){
        if($scope.maps_name==1) {
        $(".leaflet-draw-edit-remove").css("display","block");

        $(".leaflet-draw-edit-edit").css("display","block");
        }

    $("#update").hide();
    $('#add').hide();
    $('#create').show();
    $scope.textValue  = '';
    $scope.dropValue  = '';
    $scope.orgID      = '';
    $scope.clearline();
  }

  var input_value   =  document.getElementById('pac-input');
  var sbox     =  new google.maps.places.SearchBox(input_value);
  
  // search box function
  sbox.addListener('places_changed', function() {
    marker.setMap(null);
    var places = sbox.getPlaces();
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng()),
      animation: google.maps.Animation.BOUNCE,
      map: googleMap,
    });
    console.log(' lat lan  '+places[0].geometry.location.lat(), places[0].geometry.location.lng());
    googleMap.setCenter(new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng()));
    googleMap.setZoom(13);
  });


   //create button click
  $scope.drawline   =   function()
  {
    $scope.toast    = '';
    console.log($scope.textValue);
    if(!$scope.textValue){
    $scope.toast = "Please Enter Site Name ";
    toastMsg();
      
  }
    
    if(checkXssProtection($scope.textValue) == true)
    try
    {
    if($scope.map_change==0){
      shapes[0].setEditable(false);
    }
      
      var URL_ROOT    = "AddSiteController/";    /* Your website root URL */
      var text        = $scope.textValue;
      var drop        = $scope.dropValue;
      var org         = $scope.orgID;
      // post request
      if(text && drop && latlanList.length>=3 && org)
      { 
        startLoading();
       $http.post(URL_ROOT+'store', {
          '_token': $('meta[name=csrf-token]').attr('content'),
          'siteName': text,
          'siteType': drop,
          'org':org,
          'latLng': latlanList,
        })
            .success(function (response) {
              console.log("success");
                  $scope.toast = translate("Sucessfully Created")+" ...";
                    if($scope.maps_name==1) {
                    $(".leaflet-draw-edit-remove").css("display","none");
                      $(".leaflet-draw-edit-edit").css("display","none");
                      }
          serviceCall();
          toastMsg();
          $('#add').show();
          $('#create').hide();
          stopLoading();
             })
            .error(function (response) {
              console.log("fail");
              stopLoading();
            });
        }
         else
       {
        $scope.toast =translate("Enter all the field / Mark the Site");
        toastMsg();
        stopLoading();
       }

    } catch (err)
    {
      console.log(err)
      $scope.toast =translate("Enter all the field / Mark the Site");
      toastMsg();
      stopLoading();
    }
    stopLoading();
    
  }

  //create new json
  function serviceCall()
  {
    //$scope.orgIdlist    = [];

    var url_site          = GLOBAL.DOMAIN_NAME+'/viewSite';
    $http.get(url_site).success(function(response){
      console.log(response);
      mergeList       = [];
      dropDown=[];
      $scope.dropDownList=response;
      if(response != "" && response.siteParent != undefined)
      for(var i = 0; response.siteParent.length>i; i++)
      {
        if(response.siteParent[i]!=undefined)
        {
          for(var j = 0; response.siteParent[i].site.length>j; j++)
            {
              mergeList.push({'siteName': response.siteParent[i].site[j].siteName, 'siteType' : response.siteParent[i].site[j].siteType, 'latLng' : response.siteParent[i].site[j].latLng, 'orgId' : response.siteParent[i].site[j].orgId})
            }
        }
      }
      $scope.orgIdlist    = mergeList;
      
    });
  }

  
  // draw two lat lan as line in map function
  function pointToPoint(lat, lan)
  { 
    latlanList.push(lat+":"+lan);
    var firstlat  = lat;
    var firstlan  = lan;
    var latlan    = lat+","+lan;
    if(seclan && seclan)
    {
      line = new google.maps.Polyline({
            path: [
                new google.maps.LatLng(seclat, seclan), 
                new google.maps.LatLng(firstlat, firstlan)
            ],
            strokeColor: "#008000",
            strokeOpacity: 1.0,
            strokeWeight: 3,
            icons: [{
              icon: lineSymbol,
              offset: '100%'
            }],
            map: googleMap
        });
     lineList.push(line);
    }
    seclat    = firstlat;
    seclan    = firstlan;
  }


  function clearShaps(){
    if($scope.map_change==1){
      
      drawnItems.removeLayer(shapes_osm);
      mapOSM.removeLayer(polygon);
      shapes_osm=[];
      polygon=[];
      latlanList=[];
    }
    else if ($scope.map_change==0){
    for (var i = 0; i < shapes.length; i++) {
      shapes[i].setMap(null);
    }
   }

  }

  //clear button on map
  $scope.clearline  =   function()
  { 
            clearShaps();
          if($scope.map_change==0){
             
          // if (drawingManager.getDrawingMode() != null) {
                
          //       shapes = [];
          //   }
          // drawingManager.setMap();
          // console.log(shapes);
          // marker.setMap(null);
          if($scope.marker)
            $scope.marker.setMap(null);
          if(polygenColor)
            polygenColor.setMap(null);
          if (lineList){
            for (var i=0; i<lineList.length; i++){
              lineList[i].setMap(null);
             }
          }
          latlanList    = [];
          polygenList   = [];
          seclat        = '';
          seclan        = '';
          // $scope.textValue    = '';
          // $scope.dropValue    = '';
          // $scope.orgID        = '';
      }
      if($scope.maps_name==1) {
      $(".leaflet-draw-edit-remove").css("display","block");

      $(".leaflet-draw-edit-edit").css("display","block");
      }


  }
  function centerMarker(listMarker){
    var bounds = new google.maps.LatLngBounds();
    for (i = 0; i < listMarker.length; i++) {
          bounds.extend(listMarker[i]);
      }
    return bounds.getCenter()
  }
 

  function polygenFunction(list){
    
    if(list.length>0){
      var sp;
      polygenList   = [];
      polygenListOSM   = [];
      $scope.clearline();
      for(var i = 0; list.length>i; i++)
      {
          sp    = list[i].split(":");
          polygenList.push(new google.maps.LatLng(sp[0], sp[1]));
          latlanList.push(sp[0]+":"+sp[1]);
          polygenListOSM.push(new L.LatLng(parseFloat(sp[0]),parseFloat(sp[1])));
          seclat        = sp[0];
          seclan        = sp[1];
      }

      
  if($scope.map_change==1){
     // create a red polygon from an array of LatLng points
     // console.log(polygenListOSM);
     // var latlngs = [[14.072644954380316,76.497802734375],[14.594215561943921,77.53051757812499],[13.261333170798274,77.6953125],[14.072644954380316,76.497802734375]];
     // var polygon = L.polygon(polygenListOSM, {color: 'grey'}).addTo(map);
     // // zoom the map to the polygon
     // mapOSM.fitBounds(polygon.getBounds());

    //  var route = [
   //    new L.LatLng(14.072644954380316,76.497802734375),
   //    new L.LatLng(14.594215561943921,77.53051757812499),
   //    new L.LatLng(13.261333170798274,77.6953125),
   //    new L.LatLng(14.072644954380316,76.497802734375)
   // ];

     polygon = new L.Polygon(polygenListOSM, {color: 'black',weight: 1}).bindPopup($scope.textValue);

     mapOSM.addLayer(polygon);
     drawnItems.addLayer(polygon);
     mapOSM.fitBounds(polygon.getBounds());
     //mapOSM.removeLayer(polygon);


  }
  else{

      polygenColor = new google.maps.Polygon(editableColor)
            // editable: true,
            // path: polygenList,
            // strokeColor: "#282828",
            // strokeWeight: 1,
            // fillColor: '#808080',
            // fillOpacity: 0.50,
            // map: googleMap
        // });
      
        polygenColor.setPath(polygenList);
        polygenColor.setMap(googleMap);


      var latlngbounds = new google.maps.LatLngBounds();
      for (var i=0; i<polygenColor.getPath().length; i++) {
        
        var point = new google.maps.LatLng(polygenColor.getPath().i[i].lat(), polygenColor.getPath().i[i].lng());
        latlngbounds.extend(point);
    }
    console.log(latlngbounds);
    


      var labelAnchorpos = new google.maps.Point(19, 0);  ///12, 37
      $scope.marker = new MarkerWithLabel({
         position: centerMarker(polygenList), 
         map: googleMap,
         icon: 'assets/imgs/area_img.png',
         labelContent: $scope.textValue,
         labelAnchor: labelAnchorpos,
         labelClass: "labels", 
         labelInBackground: false
      });
      // googleMap.setCenter(centerMarker(polygenList)); 
      // googleMap.setZoom(14);  
      googleMap.fitBounds(latlngbounds);

    
    googleMap.setCenter(latlngbounds.getCenter());
    }
}
  }

  // $scope.changeColor  = function(){
  //   return "black"
  // }

  
  //inside the table td click function
  $scope.siteNameClick  =   function(user)
  { 
    $('#add').show();
    $('#create').hide();
    $("#update").show();
    if($scope.map_change==0)clearShaps();
    drawingManager.set('drawingMode'); 
    oldName             = user.siteName;
    $scope.textValue    = user.siteName;
    $scope.dropValue    = user.siteType;
    $scope.orgID        = user.orgId;
    var split           = user.latLng.split(",");
    polygenFunction(split);
   if($scope.map_change==0){
    
    polygenColor.getPaths().forEach(function (path, index) {
      console.log(path);
      google.maps.event.addListener(path, 'insert_at', function () {
        console.log('insert_at event');
        listLatLng(path.b);
        $("#update").attr('class', 'black');
      });

      google.maps.event.addListener(path, 'remove_at', function (s, t) {
        console.log('remove_at event');
        listLatLng(path.b);
        $("#update").attr('class', '');
      });

      google.maps.event.addListener(path, 'set_at', function () {
        console.log('set_at event');
        listLatLng(path.b);
        $("#update").attr('class', 'black');
        // $scope.changeColor();
        
      });
    });
}

  }

  //join the add site function
  function drawlineJoin(list)
  {
    if(list.length>0)
    {
     $scope.clearline();
     var sp;
      for(var i = 0; list.length>i; i++)
      {
          sp    = list[i].split(":");
          pointToPoint(sp[0], sp[1]);
      }
      sp = list[0].split(":");
      pointToPoint(sp[0], sp[1]);
      googleMap.setCenter(new google.maps.LatLng(sp[0], sp[1])); 
      googleMap.setZoom(11);
      
    }
  }

  // function for the update button
  $scope.updateDrawline   =   function()
  {

      if($scope.maps_name==1) {
      $(".leaflet-draw-edit-remove").css("display","block");

      $(".leaflet-draw-edit-edit").css("display","block");
      }

    startLoading();
    $scope.toast    =  '';
    var URL_ROOT    = "AddSiteController/";    /* Your website root URL */
    var text        = $scope.textValue;
    var drop        = $scope.dropValue;
    var org         = $scope.orgID;
    if(checkXssProtection($scope.textValue) == true)
    try
    {
      if(text && drop && latlanList.length>=3 && org && oldName)
      {
         $http.post(URL_ROOT+'update', {
          '_token': $('meta[name=csrf-token]').attr('content'),
          'siteName': text,
          'siteNameOld': oldName,
          'siteType': drop,
          'org':org,
          'latLng': latlanList,
        })
            .success(function (response) {
              console.log("success");
                  $scope.toast = translate("Sucessfully_Updated")+" ...";
          serviceCall();
          toastMsg();
          $("#update").attr('class', '');
          stopLoading();
             })
            .error(function (response) {
              console.log("fail");
              stopLoading();
            });
       }
       else
       {
        $scope.toast =translate("Enter all the field / Mark the Site");
        toastMsg();
        stopLoading();
       }
    }
    catch(err)
    { 
      console.log(err);
      $scope.toast =translate("Enter all the field / Mark the Site");
      toastMsg();
      stopLoading();
    }
    stopLoading();
    
  }


 $scope.exportDataCSV = function (data) {
    // console.log(data);
    $scope.csv=true;
    CSV.begin('#'+data).download(data+'.csv').go();
    $scope.csv=false;
    };

  //delete function 
  $scope.deleteClick  =   function(response)
  {
    startLoading();
    $scope.toast  = '';
    var URL_ROOT    = "AddSiteController/";  
    if(response)
    {
      $http.post(URL_ROOT+'delete', {
        '_token': $('meta[name=csrf-token]').attr('content'),
        'org':response.orgId,
        'siteName': response.siteName,
      })
            .success(function (response) {
              console.log("success");
                  $scope.toast = "Sucessfully Deleted ...";
        serviceCall();
        toastMsg();
        $scope.AddClear();
        stopLoading();
             })
            .error(function (response) {
              console.log("fail");
              stopLoading();
            });

     
     
    }
  }

$(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
    //menu loading
    $("#testLoad").load("../public/menu");
}]);


