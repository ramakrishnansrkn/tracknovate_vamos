app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){
   //var vid=newtab();
  //alert(vid);
  $scope.fmsactive=true;
  $scope.FMSoperation='view';
  //$scope.reminder=false;
  var docType;
  var tab = getParameterByName('tn');
  $scope.fms = getParameterByName('fms');
  $scope.fmsDataList=[];
  $scope.docTypeList=[];  
  $scope.Services=false;   

  function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations = [];
  $scope.url       = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  console.log($scope.url);
  $scope.gIndex    = 0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
    $scope.trimColon = function(textVal) {

      if(textVal){
       var spltVal = textVal.split(":");
       return spltVal[0];
      }
    }

  function sessionValue(vid, gname){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
  }
  
  function getTodayDate(date) {
      var date = new Date(date);
      return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
    };

  //   //get the value from the ui
  // function getUiValue(){
  //     $scope.uiDate.duedate   = $('#dateFrom').val();
  //     var endDay = new Date($scope.uiDate.duedate);
  // }

  function webCall(){
             if($scope.FMSoperation=='save'||$scope.FMSoperation=='update')
                   $scope.fmsUrl=GLOBAL.DOMAIN_NAME+'/getFmsDetails?vehicleId='+$scope.vehId+'&documentType='+$scope.docType+'&dueDate='+$scope.duedate+'&notifyInterval='+$scope.notifyInterval+'&monthInterval='+$scope.monInterval+'&amount='+$scope.amount+'&issuingCompany='+$scope.companyName+'&note='+$scope.desc;
             else if($scope.FMSoperation=='delete')
                   $scope.fmsUrl=GLOBAL.DOMAIN_NAME+'/getFmsDetails?vehicleId='+$scope.vehId+'&documentType='+docType;
             else 
                   $scope.fmsUrl=GLOBAL.DOMAIN_NAME+'/getFmsDetails?vehicleId='+$scope.vehId;

        console.log($scope.fmsUrl);       
        $http.get($scope.fmsUrl).success(function(data){
          stopLoading();
          $scope.data = [];
          $scope.data = data;
        switch($scope.fms){
          case 'FleetManagement':
           $("#fleet").addClass("active");
                    $scope.fmsDataList=data.getFmsData; //dropdown
                    $scope.Services=false;
                    $scope.docTypeList=[];
                    var remainingList=[];
                    remainingList=data.remainingList
                  //$scope.monthList=[];
                  $scope.tabactive=true;  
                  $scope.reminder=false; 
                  
                    //alert(remainingList.length);
                    for(i=0;i<remainingList.length;i++){
                      type=remainingList[i].split(":")[0];
                      //alert(type);
                      month=remainingList[i].split(":")[1];
                      $scope.docTypeList.push({'month' : month, 'type' : type});
                      //$scope.monthList.push({type : month});
                    
                  }
           break;
          case 'Reminders':
                // reminder 
                 $("#reminder").addClass("active");
                  $scope.getReminderList=data.getReminderList;
                  $scope.fmsDataList=data.serviceReminderList;
                  $scope.Services=false;
                  $scope.FleetManagement=false;
                  $scope.reminder=true;
                  $scope.tabactive=true;  
                 
            break;
          case 'Services':
                  //service
                  $("#serv").addClass("active");
                  $scope.fmsDataList=data.serviceList;
                  console.log(data.serviceList);
                  $scope.Services=true;
                  $scope.tabactive=true; 
                  $scope.FleetManagement=false;
                  $scope.reminder=false; 
                  $scope.docTypeList=[];
                  document.getElementById("dueInterval").placeholder = "Odometer Due";
                  if(($scope.FMSoperation=='view')&&(data.getServiceList!=null)){
                    for(i=0;i<data.getServiceList.length;i++){
                    $scope.docTypeList.push({'type' : data.getServiceList[i]});
                  }
                }
          break;
          default:
                  $scope.fmsDataList=data.getFmsData;
                  //$scope.docTypeList=data.remainingList;
            break;
      
        }

        console.log($scope.fmsDataList);
      }); 
        stopLoading();
  }


  // initial method
  //http://188.166.244.126:9000/getPeopleCountData?userId=SENTINI&vehicleId=TESTINGPEOPLECOUNTER&duedateTime=1536308634000&toDateTime=1536648657000
  $scope.$watch("url", function (val) {
    vamoservice.getDataCall($scope.url).then(function(data) {
      console.log(data);
    //startLoading();
    // $scope.group_list=[];
    //   $scope.group_list=grouplist(data);
      $scope.vehicle_group=[];
      $scope.vehicle_list = data;

      if(data.length){
        var user = JSON.parse(localStorage.getItem('user'));
        var vehgp=null;
        var vid=null;
        var gname=null;
        console.log(user);
        if(user!=undefined){
           vehgp     =  user.split(',');
           vid    =  vehgp[0];
           gname  =  vehgp[1];
           console.log(vid);
           console.log(gname);

        }
        if(vid!=null && gname!=null){
          
           $scope.vehiname=vid;
           $scope.uiGroup=gname;
           $scope.gName  =gname;
        }else{
           $scope.vehiname = data[0].vehicleLocations[0].vehicleId;
           $scope.uiGroup  = data[0].group;
           $scope.gName  = data[0].group;
        }
        angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
          if($scope.gName == val.group){
            $scope.gIndex = val.rowId;
            $scope.groupid=$scope.gIndex;
            angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                if($scope.vehiname == value.vehicleId){
                  $scope.shortNam = value.shortName;
                  $scope.vehId   = value.vehicleId;
                }
            })
          }
        });
        sessionValue($scope.vehiname, $scope.gName)
      }
      
      
      startLoading();
      webCall();
      //getAlerts(data[$scope.gIndex].vehicleLocations);
    //stopLoading();
    }); 
  });

    
  $scope.groupSelection   = function(groupName, groupId) {
    startLoading();
    $scope.addedit='';
    $scope.gName   = groupName;
    $scope.uiGroup = $scope.trimColon(groupName);
    $scope.gIndex  = groupId;
    $scope.groupid=$scope.gIndex;
    var url        = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

    vamoservice.getDataCall(url).then(function(response){
    
      $scope.vehicle_list = response;
      $scope.shortNam     = response[$scope.gIndex].vehicleLocations[0].shortName;
      $scope.vehiname     = response[$scope.gIndex].vehicleLocations[0].vehicleId;
      sessionValue($scope.vehiname, $scope.gName);
      //console.log(response);
        angular.forEach(response, function(val, key){
          if($scope.gName == val.group){
          //$scope.gIndex = val.rowId;
             angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys){
                if($scope.vehiname == value.vehicleId) {
                  $scope.shortNam = value.shortName;
                  $scope.vehId   = value.vehicleId;
                }
            });
            }
        });

      //getUiValue();
      webCall();
      getAlerts(response[$scope.gIndex].vehicleLocations);
      //stopLoading();
    });

  }

  $scope.changeFMS = function(fms){
   var href='fleetManagement?fms='+fms;
   window.location.href = href;
 }

  $scope.genericFunction  = function (vehid, index){
    startLoading();
    $scope.addedit='';
    $scope.vehiname = vehid;
    sessionValue($scope.vehiname, $scope.gName)
    angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
      if(vehid == val.vehicleId){
        $scope.shortNam = val.shortName;
          $scope.vehId   = val.vehicleId;
      }
    });
    //getUiValue();
    webCall();
  }

  $scope.submitFunction   = function(){
    startLoading();
  //$scope.interval="";
    getUiValue();
    webCall();
  //webServiceCall();
    //stopLoading();
  }

  $scope.exportData = function (data) {
    //console.log(data);
    var blob = new Blob([document.getElementById(data).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
  };

  $scope.exportDataCSV = function (data) {
    //console.log(data);
      CSV.begin('#'+data).download(data+'.csv').go();
  };
  $scope.updateFMS   = function(vehicleId,op,preop){
      $scope.error="";
      $scope.error2="";
      $scope.FMSoperation=op;
      console.log($scope.docType);
   
          if($scope.docType==undefined||$scope.notifyInterval==undefined||$scope.companyName==undefined||$scope.amount==undefined||$scope.desc==undefined||$scope.docType==""||$scope.notifyInterval==""||$scope.companyName==""||$scope.amount==""||$scope.desc=="")
                              {
                                    $scope.error='Please Enter all Field';
                                    $scope.FMSoperation=preop;
          }
          else{
                    var duedatemilisec = new Date($scope.duedate).getTime();
                    var todaymilisec= new Date().getTime(); 
                    var millisecondsPerDay = 1000 * 60 * 60 * 24;
                    var day=((duedatemilisec-todaymilisec)/millisecondsPerDay);
                    var diff=Math.floor(day)-$scope.notifyInterval;
                    //alert(diff);
                    console.log('day diff'+diff);
                          if(diff<0&&($scope.Services==false)){
                          $scope.error='Notify Interval should be less than two days of Due date';
                          $scope.FMSoperation=preop;
                          }else{
                                  if($scope.amount>0){
                                  webCall();
                                  $scope.addedit='';
                                  $scope.FMSoperation='view';
                                  }else{
                                    $scope.error='Enter valid Amount';
                                  }
                            }
                }
           }
   $scope.saveFMS   = function(vehicleId,documentType){
        $scope.FMSoperation='update';
               if($scope.docType==undefined||$scope.notifyInterval==undefined||$scope.companyName==undefined||$scope.amount==undefined||$scope.desc==undefined){
                          $scope.error='Please enter all Field';
                          $scope.FMSoperation='edit';
                }
                else{
                        webCall();
                        $scope.addedit='';
                        $scope.FMSoperation='view';
                }
      }
   $scope.deleteFMS = function(vehicleId,documentType){
         $scope.error='';
         $scope.error2='';
         $scope.FMSoperation='delete';
         $scope.addedit='';
         $scope.vehId=vehicleId;
         docType=documentType;
         var choice=confirm('Are you sure you want to delete?');
         if(choice){
         webCall();
             }
         $scope.FMSoperation='view';
      }
console.log($scope.vehId);
   $scope.changedValue = function(type){
      if($scope.fms=='FleetManagement'){
            $scope.monInterval=$scope.documentType.month;
          }
            $scope.docType=$scope.documentType.type;
   }

  $scope.editFMS   = function(vehicleId,FMSData){
      $scope.action='Edit FMS Report';
      $scope.error="";
      $scope.error2="";
      $scope.addedit='true';
      $scope.FMSoperation='edit';
         if($scope.FMSoperation=='edit'){  
                              var editfmsdata=[];
                              var editfmsdata=FMSData;
                              $scope.type=editfmsdata.documentType;
                              //$scope.documentType=editfmsdata.documentType;
                              $scope.docType=editfmsdata.documentType;
                              $scope.duedate=editfmsdata.dueDate;
                              $scope.companyName=editfmsdata.companyName;
                              $scope.amount=editfmsdata.amount;
                              $scope.desc=editfmsdata.description;
                              if($scope.fms=='Services'){
                              $scope.notifyInterval=editfmsdata.odometerDue;
                              $scope.monInterval="";
                            }
                              else{
                              $scope.notifyInterval=editfmsdata.notifyInterval;
                              $scope.monInterval=editfmsdata.monthInterval;
                            }
                            $scope.docTypeList=[];
                            // $scope.docTypeList.push({'month' : $scope.monInterval, 'type' : $scope.type});
                              
          }
   }

  $scope.addFMS   = function(vehicleId){
    $scope.action='Add FMS Report';
    $scope.error="";
    $scope.error2="";
    if(($scope.fmsDataList!=null)&&($scope.docTypeList.length==0)){ 
       $scope.error2='All Services Entered';
     }
    else{
    $scope.addedit='true';
    $scope.FMSoperation='add';
    var dateObj       =  new Date();
    if($scope.fms=='Services')
      $scope.fromNowTS    =  new Date(dateObj.setDate(dateObj.getDate()+1));
    else
      $scope.fromNowTS    =  new Date(dateObj.setDate(dateObj.getDate()+2));
    $scope.duedate   =  ($scope.fromNowTS).toISOString().slice(0, 10);
                            $scope.type="";
                            $scope.monInterval="";
                            $scope.notifyInterval="";
                            $scope.companyName="";
                            $scope.amount="";
                            $scope.desc="";
      }
  
  }

     function sessionValue(vid, gname){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    $("#testLoad").load("../public/menu");
  }

  $('#minus').click(function(){
    $('#menu').toggle(1000);
  })

}]);
